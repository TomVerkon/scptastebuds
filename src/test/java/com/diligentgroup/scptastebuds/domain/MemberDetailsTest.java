//package com.diligentgroup.scptastebuds.domain;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import org.junit.jupiter.api.Test;
//import org.thymeleaf.util.StringUtils;
//
//import com.diligentgroup.scptastebuds.TestValues;
//
//class MemberDetailsTest extends TestValues {
//
//	@Test
//	void testNoArgConstructor() {
//		MemberDetails member = new MemberDetails();
//		assertNull(member.getId());
//		assertNull(member.getEmail());
//		assertNull(member.getPassword());
//		assertNull(member.getFirstName());
//		assertNull(member.getLastName());
//		assertNull(member.getPhone());
//		assertEquals(MemberStatus.PENDING, member.getMemberStatus());
//		assertNull(member.getRenewalDate());
//		assertNull(member.getCreatedDateTime());
//		assertNull(member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testIsNew() {
//		MemberDetails member = new MemberDetails();
//		assert (member.isNew());
//		member.setId(id);
//		assertFalse(member.isNew());
//	}
//
//	@Test
//	void testAllArgConstructor() {
//		MemberDetails member = new MemberDetails(id, email, password, firstName, lastName, phone, memberStatus, renewalDate,
//				createdDateTime, lastUpdatedDateTime);
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testBuilder() {
//		MemberDetails member = MemberDetails.builder().id(id).email(email).password(password).firstName(firstName).lastName(lastName)
//				.phone(phone).memberStatus(memberStatus).renewalDate(renewalDate).createdDateTime(createdDateTime)
//				.lastUpdatedDateTime(lastUpdatedDateTime).build();
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testSetters() {
//		MemberDetails member = new MemberDetails();
//		member.setId(id);
//		member.setEmail(email);
//		member.setFirstName(firstName);
//		member.setLastName(lastName);
//		member.setPhone(phone);
//		member.setPassword(password);
//		member.setMemberStatus(memberStatus);
//		member.setRenewalDate(renewalDate);
//		member.setCreatedDateTime(createdDateTime);
//		member.setLastUpdatedDateTime(lastUpdatedDateTime);
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testBuilderDefault() {
//		MemberDetails member = MemberDetails.builder().build();
//		assertEquals(MemberStatus.PENDING, member.getMemberStatus());
//	}
//
//	@Test
//	void testToString() {
//		MemberDetails member = MemberDetails.builder().id(id).email(email).password(password).firstName(firstName).lastName(lastName)
//				.phone(phone).memberStatus(memberStatus).renewalDate(renewalDate).createdDateTime(createdDateTime)
//				.lastUpdatedDateTime(lastUpdatedDateTime).build();
//		String memberToString = member.toString();
//		assert (StringUtils.contains(memberToString, "id=" + id));
//		assert (StringUtils.contains(memberToString, "email=" + email));
//		assert (StringUtils.contains(memberToString, "password=" + password));
//		assert (StringUtils.contains(memberToString, "firstName=" + firstName));
//		assert (StringUtils.contains(memberToString, "lastName=" + lastName));
//		assert (StringUtils.contains(memberToString, "phone=" + phone));
//		assert (StringUtils.contains(memberToString, "memberStatus=" + memberStatus));
//		assert (StringUtils.contains(memberToString, "renewalDate=" + renewalDate));
//		assert (StringUtils.contains(memberToString, "createdDateTime=" + createdDateTime));
//		assert (StringUtils.contains(memberToString, "lastUpdatedDateTime=" + lastUpdatedDateTime));
//	}
//}

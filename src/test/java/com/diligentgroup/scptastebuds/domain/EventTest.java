//package com.diligentgroup.scptastebuds.domain;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import org.junit.jupiter.api.Test;
//import org.thymeleaf.util.StringUtils;
//
//import com.diligentgroup.scptastebuds.TestValues;
//
//class EventTest extends TestValues {
//
//	@Test
//	void testNoArgsConstructor() {
//		Event event = new Event();
//		assertNull(event.getId());
//		assertNull(event.getEventDate());
//		assertNull(event.getEventEndTime());
//		assertNull(event.getEventStartTime());
//		assertNull(event.getLocation());
//		assertNull(event.getName());
//		assertNull(event.getUrl());
//		assertEquals(EventStatus.PENDING, event.getEventStatus());
//		assertNull(event.getMaxAttendees());
//		assertNull(event.getTicketCost());
//		//assertEquals(event.getAttendees().size(), 0);
//
//	}
//
//	@Test
//	void testIsNew() {
//		Event event = new Event();
//		assert (event.isNew());
//		event.setId(id);
//		assertFalse(event.isNew());
//	}
//
//	@Test
//	void testAllArgsConstructorAndGetters() {
//		Event event = new Event(id, name, url, location, eventDate.toString(), eventStartTime.toString(),
//				eventEndTime.toString(), EventStatus.CLOSED, maxAttendees, ticketCost);
//		assertEquals(id, event.getId());
//		assertEquals(name, event.getName());
//		assertEquals(url, event.getUrl());
//		assertEquals(location, event.getLocation());
//		assertEquals(eventDate, event.getEventDate());
//		assertEquals(eventStartTime, event.getEventStartTime());
//		assertEquals(eventEndTime, event.getEventEndTime());
//		assertEquals(EventStatus.CLOSED, event.getEventStatus());
////		assertEquals(maxAttendees, event.getMaxAttendees());
//		assert (ticketCost.equals(event.getTicketCost()));
////		assertEquals(event.getAttendees().size(), 0);
//	}
//
//	@Test
//	void testBuilder() {
//		Event event = Event.builder().id(id).name(name).url(url).location(location).eventDate(eventDate.toString())
//				.eventStartTime(eventStartTime.toString()).eventEndTime(eventEndTime.toString())
//				.eventStatus(EventStatus.CLOSED).maxAttendees(maxAttendees).ticketCost(ticketCost).build();
//		assertEquals(id, event.getId());
//		assertEquals(name, event.getName());
//		assertEquals(url, event.getUrl());
//		assertEquals(location, event.getLocation());
//		assertEquals(eventDate, event.getEventDate());
//		assertEquals(eventStartTime, event.getEventStartTime());
//		assertEquals(eventEndTime, event.getEventEndTime());
//		assertEquals(EventStatus.CLOSED, event.getEventStatus());
//		assertEquals(maxAttendees, event.getMaxAttendees());
//		assert (ticketCost.equals(event.getTicketCost()));
//		//assertNotNull(event.getAttendees());
//		//assertEquals(event.getAttendees().size(), 0);
//	}
//
//	@Test
//	void testSetters() {
//		Event event = new Event();
//		event.setId(id);
//		event.setEventDate(eventDate.toString());
//		event.setEventEndTime(eventEndTime.toString());
//		event.setEventStartTime(eventStartTime.toString());
//		event.setLocation(location);
//		event.setName(name);
//		event.setUrl(url);
//		event.setEventStatus(EventStatus.CLOSED);
//		event.setMaxAttendees(maxAttendees);
//		event.setTicketCost(ticketCost);
//		assertEquals(id, event.getId());
//		assertEquals(eventDate.toString(), event.getEventDate());
//		assertEquals(eventEndTime.toString(), event.getEventEndTime());
//		assertEquals(eventStartTime.toString(), event.getEventStartTime());
//		assertEquals(location, event.getLocation());
//		assertEquals(name, event.getName());
//		assertEquals(url, event.getUrl());
//		assertEquals(EventStatus.CLOSED, event.getEventStatus());
//		assertEquals(maxAttendees, event.getMaxAttendees());
//		assert (ticketCost.equals(event.getTicketCost()));
//	}
//
////	@Test
////	void testAddEventAttendee() {
////		EventAttendee eventAttendee = EventAttendee.builder().attendeeId(id3).build();
////		Event event = new Event();
////		assertEquals(0, event.getAttendees().size());
////		event.addAttendee(eventAttendee);
////		assertEquals(1, event.getAttendees().size());
////	}
//
//	@Test
//	void testToString() {
//		Event event = Event.builder().id(id).name(name).url(url).location(location).eventDate(eventDate.toString())
//				.eventStartTime(eventStartTime.toString()).eventEndTime(eventEndTime.toString())
//				.eventStatus(EventStatus.PENDING).maxAttendees(maxAttendees).ticketCost(ticketCost).build();
//		String eventToString = event.toString();
//		assert (StringUtils.contains(eventToString, "id=" + id));
//		assert (StringUtils.contains(eventToString, "name=" + name));
//		assert (StringUtils.contains(eventToString, "url=" + url));
//		assert (StringUtils.contains(eventToString, "location=" + location));
//		assert (StringUtils.contains(eventToString, "eventDate=" + eventDate.toString()));
//		assert (StringUtils.contains(eventToString, "eventStartTime=" + eventStartTime.toString()));
//		assert (StringUtils.contains(eventToString, "eventEndTime=" + eventEndTime.toString()));
//		assert (StringUtils.contains(eventToString, "eventStatus=" + EventStatus.PENDING));
//		assert (StringUtils.contains(eventToString, "maxAttendees=" + maxAttendees.toString()));
//		assert (StringUtils.contains(eventToString, "ticketCost=" + ticketCost.toString()));
//	}
//
//}

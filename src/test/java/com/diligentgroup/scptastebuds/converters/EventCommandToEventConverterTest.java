//package com.diligentgroup.scptastebuds.converters;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.EventAttendeeCommand;
//import com.diligentgroup.scptastebuds.commands.EventCommand;
//import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
//import com.diligentgroup.scptastebuds.domain.Event;
//import com.diligentgroup.scptastebuds.domain.EventStatus;
//
//class EventCommandToEventConverterTest extends TestValues {
//
//	EventCommandToEventConverter converter;
//
//	@BeforeEach
//	void setUp() {
//		converter = new EventCommandToEventConverter(new EventAttendeeCommandToEventAttendeeConverter());
//	}
//
//	@Test
//	void testConvertNull() {
//		assertNull(converter.convert(null));
//	}
//
//	@Test
//	void testDefault() {
//		Event event = converter.convert(EventCommand.builder().build());
//		assertNotNull(event);
//		assertEquals(EventStatus.PENDING, event.getEventStatus());
//	}
//
//	@Test
//	void testIsNew() {
//		Event event = converter.convert(EventCommand.builder().build());
//		assertNotNull(event);
//		assert (event.isNew());
//	}
//
//	@Test
//	void testFullConversion() {
//		Event event = converter.convert(EventCommand.builder().eventDate(eventDate).eventEndTime(eventEndTime)
//				.eventStartTime(eventStartTime).id(id).location(location).name(name).eventStatus(EventStatus.CLOSED)
//				.url(url)
//				.eventAttendee(
//						EventAttendeeCommand.builder().attendeeId(id3).attendeeName(name).attendeeEmail(email).build())
//				.build());
//		assertEquals(eventDate, event.getEventDate());
//		assertEquals(eventEndTime, event.getEventEndTime());
//		assertEquals(eventStartTime, event.getEventStartTime());
//		assertEquals(id, event.getId());
//		assertEquals(location, event.getLocation());
//		assertEquals(name, event.getName());
//		assertEquals(EventStatus.CLOSED, event.getEventStatus());
//		assertEquals(url, event.getUrl());
//		assertEquals(1, event.getEventAttendees().size());
//		assertEquals(AttendeeStatus.ACTIVE, event.getEventAttendees().get(0).getAttendeeStatus());
//		assertEquals(id3, event.getEventAttendees().get(0).getAttendeeId());
//		assertEquals(name, event.getEventAttendees().get(0).getAttendeeName());
//		assertEquals(false, event.getEventAttendees().get(0).getPaidFlag());
//	}
//
//}

//package com.diligentgroup.scptastebuds.converters;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.EventCommand;
//import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
//import com.diligentgroup.scptastebuds.domain.Event;
//import com.diligentgroup.scptastebuds.domain.EventAttendee;
//import com.diligentgroup.scptastebuds.domain.EventStatus;
//
//class EventToEventCommandConverterTest extends TestValues {
//
//	EventToEventCommandConverter converter;
//
//	@BeforeEach
//	void setUp() {
//		converter = new EventToEventCommandConverter(new EventAttendeeToEventAttendeeCommandConverter());
//	}
//
//	@Test
//	void testConvertNull() {
//		assertNull(converter.convert(null));
//	}
//
//	@Test
//	void testDefault() {
//		EventCommand event = converter.convert(Event.builder().build());
//		assertNotNull(event);
//		assertEquals(EventStatus.PENDING, event.getEventStatus());
//	}
//
//	@Test
//	void testConvert() {
//		EventCommand event = converter
//				.convert(Event.builder().eventDate(eventDate).eventEndTime(eventEndTime).eventStartTime(eventStartTime)
//						.id(id).location(location).name(name).eventStatus(EventStatus.ACTIVE).url(url)
//						.eventAttendee(
//								EventAttendee.builder().attendeeId(id3).attendeeName(name).attendeeEmail(email).build())
//						.build());
//		assertNotNull(event);
//		assertEquals(eventDate, event.getEventDate());
//		assertEquals(eventEndTime, event.getEventEndTime());
//		assertEquals(eventStartTime, event.getEventStartTime());
//		assertEquals(id, event.getId());
//		assertEquals(location, event.getLocation());
//		assertEquals(name, event.getName());
//		assertEquals(EventStatus.ACTIVE, event.getEventStatus());
//		assertEquals(url, event.getUrl());
//		assertEquals(1, event.getEventAttendees().size());
//		assertEquals(id3, event.getEventAttendees().get(0).getAttendeeId());
//		assertEquals(name, event.getEventAttendees().get(0).getAttendeeName());
//		assertEquals(false, event.getEventAttendees().get(0).getPaidFlag());
//		assertEquals(AttendeeStatus.ACTIVE, event.getEventAttendees().get(0).getAttendeeStatus());
//	}
//}

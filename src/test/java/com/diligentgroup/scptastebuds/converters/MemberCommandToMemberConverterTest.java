//package com.diligentgroup.scptastebuds.converters;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertNull;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.MemberCommand;
//import com.diligentgroup.scptastebuds.domain.MemberDetails;
//
//class MemberCommandToMemberConverterTest extends TestValues {
//
//	MemberCommandToMemberConverter converter;
//
//	@BeforeEach
//	void setUp() {
//		converter = new MemberCommandToMemberConverter();
//	}
//
//	@Test
//	void testNull() {
//		assertNull(converter.convert(null));
//	}
//
//	@Test
//	void testIsNew() {
//		assertTrue(converter.convert(new MemberCommand()).isNew());
//		assertFalse(converter.convert(MemberCommand.builder().id(id).build()).isNew());
//	}
//
//	@Test
//	void testFullConversion() {
//		MemberCommand memberCommand = new MemberCommand(id, email, password, confirmPassword, firstName, lastName,
//				phone, memberStatus, renewalDate, createdDateTime, lastUpdatedDateTime);
//		MemberDetails member = converter.convert(memberCommand);
//		assertEquals(id, member.getId());
//		assertEquals(memberCommand.getId(), member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(memberCommand.getEmail(), member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(memberCommand.getPassword(), member.getPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(memberCommand.getFirstName(), member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(memberCommand.getLastName(), member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberCommand.getPhone(), member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(memberCommand.getMemberStatus(), member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(memberCommand.getRenewalDate(), member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(memberCommand.getCreatedDateTime(), member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//		assertEquals(memberCommand.getLastUpdatedDateTime(), member.getLastUpdatedDateTime());
//	}
//
//}

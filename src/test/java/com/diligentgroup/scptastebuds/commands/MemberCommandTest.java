//package com.diligentgroup.scptastebuds.commands;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNull;
//
//import org.junit.jupiter.api.Test;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.domain.MemberStatus;
//
//class MemberCommandTest extends TestValues {
//
//	@Test
//	void testNoArgConstructor() {
//		MemberCommand member = new MemberCommand();
//		assertNull(member.getId());
//		assertNull(member.getEmail());
//		assertNull(member.getPassword());
//		assertNull(member.getFirstName());
//		assertNull(member.getLastName());
//		assertNull(member.getPhone());
//		assertEquals(MemberStatus.PENDING, member.getMemberStatus());
//		assertNull(member.getRenewalDate());
//		assertNull(member.getCreatedDateTime());
//		assertNull(member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testAllArgConstructor() {
//		MemberCommand member = new MemberCommand(id, email, password, confirmPassword, firstName, lastName, phone,
//				memberStatus, renewalDate, createdDateTime, lastUpdatedDateTime);
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(confirmPassword, member.getConfirmPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testBuilder() {
//		MemberCommand member = MemberCommand.builder().id(id).email(email).password(password)
//				.confirmPassword(confirmPassword).firstName(firstName).lastName(lastName).phone(phone)
//				.memberStatus(memberStatus).renewalDate(renewalDate).createdDateTime(createdDateTime)
//				.lastUpdatedDateTime(lastUpdatedDateTime).build();
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(confirmPassword, member.getConfirmPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testSetters() {
//		MemberCommand member = new MemberCommand();
//		member.setId(id);
//		member.setEmail(email);
//		member.setFirstName(firstName);
//		member.setLastName(lastName);
//		member.setPhone(phone);
//		member.setPassword(password);
//		member.setConfirmPassword(confirmPassword);
//		member.setMemberStatus(memberStatus);
//		member.setRenewalDate(renewalDate);
//		member.setCreatedDateTime(createdDateTime);
//		member.setLastUpdatedDateTime(lastUpdatedDateTime);
//		assertEquals(id, member.getId());
//		assertEquals(email, member.getEmail());
//		assertEquals(password, member.getPassword());
//		assertEquals(confirmPassword, member.getConfirmPassword());
//		assertEquals(firstName, member.getFirstName());
//		assertEquals(lastName, member.getLastName());
//		assertEquals(phone, member.getPhone());
//		assertEquals(memberStatus, member.getMemberStatus());
//		assertEquals(renewalDate, member.getRenewalDate());
//		assertEquals(createdDateTime, member.getCreatedDateTime());
//		assertEquals(lastUpdatedDateTime, member.getLastUpdatedDateTime());
//	}
//
//	@Test
//	void testBuilderDefault() {
//		MemberCommand member = MemberCommand.builder().build();
//		assertEquals(MemberStatus.PENDING, member.getMemberStatus());
//	}
//}

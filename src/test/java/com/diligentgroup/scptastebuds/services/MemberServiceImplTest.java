//package com.diligentgroup.scptastebuds.services;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyList;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.security.crypto.factory.PasswordEncoderFactories;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.MemberCommand;
//import com.diligentgroup.scptastebuds.converters.MemberCommandToMemberConverter;
//import com.diligentgroup.scptastebuds.converters.MemberToMemberCommandConverter;
//import com.diligentgroup.scptastebuds.domain.MemberDetails;
//import com.diligentgroup.scptastebuds.repositories.MemberRepository;
//
//class MemberServiceImplTest extends TestValues {
//
//	MemberService service;
//
//	@Mock
//	MemberRepository memberRepository;
//
//	MemberCommandToMemberConverter memberCommandToMemberConverter = new MemberCommandToMemberConverter();
//	MemberToMemberCommandConverter memberToMemberCommandConverter = new MemberToMemberCommandConverter();
//
//	@BeforeEach
//	void setUp() {
//		MockitoAnnotations.initMocks(this);
//		service = new MemberServiceImpl(memberRepository, memberCommandToMemberConverter,
//				memberToMemberCommandConverter);
//	}
//
//	@Test
//	void testSaveAllMembers() {
//		// given
//		List<MemberCommand> inMemberCommands = new ArrayList<>();
//		inMemberCommands.add(MemberCommand.builder().firstName(firstName).password("s3cr3t").build());
//		inMemberCommands.add(MemberCommand.builder().firstName(firstName).password("s3cr3t").build());
//		List<MemberDetails> members = new ArrayList<>();
//		members.add(MemberDetails.builder().firstName(firstName).password("s3cr3t").build());
//		members.add(MemberDetails.builder().firstName(firstName).password("s3cr3t").build());
//		when(memberRepository.saveAll(anyList())).thenReturn(members);
//
//		// when
//		List<MemberCommand> memberCommands = service.saveAll(inMemberCommands);
//
//		// then
//		assertNotNull(memberCommands);
//		assertEquals(2, memberCommands.size());
//		verify(memberRepository, times(1)).saveAll(anyList());
//
//	}
//
//	@Test
//	void testGetMemberById() {
//		// given
//		MemberDetails returnedMember = MemberDetails.builder().id(id).build();
//		when(memberRepository.findById(anyLong())).thenReturn(Optional.of(returnedMember));
//		// when
//		MemberCommand command = service.findById(id);
//		// then
//		assertNotNull(command);
//		verify(memberRepository, times(1)).findById(id);
//
//	}
//
//	@Test
//	void testDeleteMember() {
//		service.deleteById(anyLong());
//		verify(memberRepository, times(1)).deleteById(anyLong());
//	}
//
//	@Test
//	void testDeleteAll() {
//		service.deleteAll();
//		verify(memberRepository, times(1)).deleteAll();
//	}
//
//	@Test
//	void testGetMemberList() {
//		// given - setup the mock repository return values
//		List<MemberDetails> members = new ArrayList<>();
//		members.add(MemberDetails.builder().id(id).build());
//		members.add(MemberDetails.builder().id(id2).build());
//
//		// when - stipulate that the repository will return the values above
//		when(memberRepository.findAll()).thenReturn(members);
//
//		// then - make the call to the service and
//		List<MemberCommand> memberComs = service.findAll();
//		// should receive to memberCommands back
//		assertEquals(2, memberComs.size());
//		// repository findAll should only be called once
//		verify(memberRepository, times(1)).findAll();
//	}
//
//	@Test
//	void testSaveUpdateExistingMember() {
//		// given
//		MemberDetails member = MemberDetails.builder().id(id).password(password).build();
//		Optional<MemberDetails> givenMember = Optional.of(member);
//
//		// when
//		when(memberRepository.findById(anyLong())).thenReturn(givenMember);
//		when(memberRepository.save(any())).thenReturn(member);
//
//		// then
//		MemberCommand memberCommand = service.save(memberToMemberCommandConverter.convert(member));
//		assertNotNull(memberCommand);
//		verify(memberRepository, times(1)).findById(anyLong());
//		verify(memberRepository, times(1)).save(any(MemberDetails.class));
//	}
//
//	@Test
//	void testSaveUpdateNewMember() {
//		// given
//		MemberDetails givenMember = MemberDetails.builder().password(password).build();
//
//		// when
//		when(memberRepository.save(any())).thenReturn(givenMember);
//
//		// then
//		MemberCommand memberCommand = service.save(memberToMemberCommandConverter.convert(givenMember));
//		assertNotNull(memberCommand);
//		verify(memberRepository, times(0)).findById(anyLong());
//		verify(memberRepository, times(1)).save(any(MemberDetails.class));
//	}
//
//	@Test
//	void testEncryption() {
//		// given
//		MemberDetails member = MemberDetails.builder().password(password).build();
//
//		// then
//		MemberDetails encryptedMember = service.encryptPassword(member);
//		assertNotEquals(password, encryptedMember.getPassword());
//		PasswordEncoderFactories.createDelegatingPasswordEncoder().matches(password, encryptedMember.getPassword());
//
//	}
//
//}

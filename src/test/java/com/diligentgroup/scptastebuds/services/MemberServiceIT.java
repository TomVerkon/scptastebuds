//package com.diligentgroup.scptastebuds.services;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.MemberCommand;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//public class MemberServiceIT extends TestValues {
//
//	@Autowired
//	MemberService service;
//
//	@Test
//	void testSaveMember() {
//		MemberCommand persistedMemberCommand = service
//				.save(MemberCommand.builder().firstName(firstName).password(password).build());
//		assertNotNull(persistedMemberCommand.getId());
//	}
//
//}

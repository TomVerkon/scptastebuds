//package com.diligentgroup.scptastebuds.services;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.EventCommand;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//public class EventServiceIT extends TestValues {
//
//	@Autowired
//	EventService service;
//
//	@Test
//	void testSaveMember() {
//		EventCommand persistedEventCommand = service.save(EventCommand.builder().name(name).build());
//		assertNotNull(persistedEventCommand.getId());
//	}
//
//}

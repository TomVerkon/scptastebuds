//package com.diligentgroup.scptastebuds.web.controllers;
//
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.security.test.context.support.WithUserDetails;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.RequestPostProcessor;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
////@ExtendWith(SpringExtension.class)
//@SpringBootTest
//class MemberControllerIT {
//
//	@Autowired
//	WebApplicationContext wac;
//
//	MockMvc mockMVC;
//
////	@MockBean
////	MemberService memberService;
////
////	@MockBean
////	MemberRepository memberRepository;
////
////	@MockBean
////	DomainCommandMapper mapper;
////
////	@MockBean
////	PDFReportService pdfService;
////	@MockBean
////	AnnualMembershipService annualMembershipService;
////
////	@MockBean
////	YearService yearService;
////
////	@MockBean
////	SelectedYearId selectedYearId;
////
////	@MockBean
////	EventAttendeeService eventAttendeeService;
////
////	@MockBean
////	EventService eventService;
//
////	RequestPostProcessor adminRPP;
////	RequestPostProcessor cfoRPP;
////	RequestPostProcessor bdmbrRPP;
//
//	@BeforeEach
//	void setUp() throws Exception {
//		mockMVC = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
////		adminRPP = httpBasic("admin", "admin");
////		cfoRPP = httpBasic("cfo", "cfo");
////		bdmbrRPP = httpBasic("bdmbr", "bdmbr");
//	}
//
//	@Test
//	@WithMockUser("admin")
//	void getMembersListADMIN() throws Exception {
//		mockMVC.perform(get("/member/list")).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithUserDetails(value = "cfo", userDetailsServiceBeanName = "TBUserDetailsService")
//	void getMembersListCFO() throws Exception {
//		mockMVC.perform(get("/member/list")).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithUserDetails(value = "bdmbr", userDetailsServiceBeanName = "TBUserDetailsService")
//	void getMembersListBDMBR() throws Exception {
//		mockMVC.perform(get("/member/list")).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithMockUser("admin")
//	void addMemberADMIN() throws Exception {
//		mockMVC.perform(get("/member/add")).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithUserDetails(value = "cfo", userDetailsServiceBeanName = "TBUserDetailsService")
//	void addMemberCFO() throws Exception {
//		mockMVC.perform(get("/member/add")).andExpect(status().isForbidden());
//	}
//
//	@Test
//	@WithMockUser("bdmbr")
//	void addMemberDBMBR() throws Exception {
//		mockMVC.perform(get("/member/add")).andExpect(status().isForbidden());
//	}
//	
//	@Test
//	@WithMockUser("admin")
//	void saveUpdateMemberADMIN() throws Exception {
//		mockMVC.perform(post("/member/save")).andExpect(status().isOk());
//	}
//
//}

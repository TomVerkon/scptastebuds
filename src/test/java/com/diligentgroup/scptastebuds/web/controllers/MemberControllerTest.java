package com.diligentgroup.scptastebuds.web.controllers;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.diligentgroup.scptastebuds.bootstrap.DataLoader;
import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.repositories.AnnualMembershipRepository;
import com.diligentgroup.scptastebuds.repositories.MemberRepository;
import com.diligentgroup.scptastebuds.services.MemberService;
import com.diligentgroup.scptastebuds.services.YearService;

@SpringBootTest
class MemberControllerTest {

	@Autowired
	WebApplicationContext wac;

	protected MockMvc mockMvc;

//    @MockBean
//    BeerRepository beerRepository;
//
//    @MockBean
//    BeerInventoryRepository beerInventoryRepository;
//
//    @MockBean
//    BreweryService breweryService;
//
//    @MockBean
//    CustomerRepository customerRepository;
//
	@MockBean
	MemberService memberService;
	@MockBean
	MemberRepository repository;
	@MockBean
	AnnualMembershipRepository membershipRepository;
	@MockBean
	YearService yearService;
	@MockBean
	DataLoader dataLoader;


	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
	}

	@Test
	void testDeleteMemberAdmin() throws Exception {
		mockMvc.perform(delete("/member/1/delete").with(httpBasic("dverkon@gmail.com", "CupCake")))
				.andExpect(status().isForbidden());
		;
	}

//	@Test
//	void testGetMemberList() throws Exception {
//		when(service.findAll()).thenReturn(memberCommands);
//		mockMvc.perform(get("/member/list")).andExpect(status().is(200)).andExpect(view().name("/member/member-list"))
//				.andExpect(model().attribute("members", hasSize(3)));
//
//	}
//
//	@Test
//	void testAddMember() throws Exception {
//		mockMvc.perform(get("/member/add")).andExpect(status().isOk()).andExpect(view().name("/member/member-form"))
//				.andExpect(model().attribute("memberCommand", isA(MemberCommand.class)))
//				.andExpect(model().attribute("action", "Add")).andExpect(model().attribute("active", "member"));
//	}
//
//	@Test
//	void testSaveUpdateMemberHappyPath() throws Exception {
//		when(service.save(any(MemberCommand.class))).thenReturn(
//				MemberCommand.builder().id(id).email(email).firstName(firstName).lastName(lastName).build());
//		mockMvc.perform(post("/member/save").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("id", "")
//				.param("firstName", firstName).param("lastName", lastName).param("email", email))
//				.andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/member/" + id + "/view"));
//	}
//
//	@Test
//	void testSaveUpdateMemberFailValidation() throws Exception {
//		mockMvc.perform(post("/member/save").contentType(MediaType.APPLICATION_FORM_URLENCODED).param("id", ""))
//				.andExpect(view().name("/member/member-form"));
//	}
//
//	@Test
//	void testEditMember() throws Exception {
//		when(service.findById(anyLong())).thenReturn(MemberCommand.builder().id(id).build());
//		mockMvc.perform(get("/member/1/edit")).andExpect(status().isOk()).andExpect(view().name("/member/member-form"))
//				.andExpect(model().attribute("memberCommand", isA(MemberCommand.class)))
//				.andExpect(model().attribute("action", "Edit")).andExpect(model().attribute("active", "member"));
//	}
//
//	@Test
//	void testViewMember() throws Exception {
//		when(service.findById(anyLong())).thenReturn(MemberCommand.builder().id(id).build());
//		mockMvc.perform(get("/member/1/view")).andExpect(status().isOk()).andExpect(view().name("/member/member-form"))
//				.andExpect(model().attribute("memberCommand", isA(MemberCommand.class)))
//				.andExpect(model().attribute("action", "View")).andExpect(model().attribute("active", "member"));
//	}
//
}

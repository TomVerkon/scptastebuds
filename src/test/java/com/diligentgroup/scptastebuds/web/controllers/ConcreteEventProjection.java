package com.diligentgroup.scptastebuds.web.controllers;

import java.util.List;

import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.domain.EventStatus;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;
import com.diligentgroup.scptastebuds.repositories.projections.EventProjection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConcreteEventProjection implements EventProjection {
	
	private Long eventId;
	
	private Long eventVersion;

	private List<EventAttendee> eventAttendees;

	private Integer yearId;

	private String name;

	private String eventDate; // in the format of yyyy-mm-dd

	private EventStatus eventStatus = EventStatus.PENDING;
	
	private Integer maxAttendees;
	
	private Integer ticketsSold;

	private Integer ticketsReserved;

	private EventAttendeeProjectionList eventAttendeeProjectionList;
}

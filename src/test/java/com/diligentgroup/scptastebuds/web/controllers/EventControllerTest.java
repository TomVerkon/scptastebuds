package com.diligentgroup.scptastebuds.web.controllers;
//package com.diligentgroup.scptastebuds.controllers;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.isA;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//
//import com.diligentgroup.scptastebuds.TestValues;
//import com.diligentgroup.scptastebuds.commands.EventCommand;
//import com.diligentgroup.scptastebuds.services.EventService;
//
//class EventControllerTest extends TestValues {
//
//	@InjectMocks
//	EventController controller;
//
//	@Mock
//	EventService service;
//
//	List<EventCommand> eventCommands = new ArrayList<>();
//	MultiValueMap<String, String> values = new LinkedMultiValueMap<>();
//
//	MockMvc mockMvc;
//
//	@BeforeEach
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
//		eventCommands.add(EventCommand.builder().id(id).eventDate(eventDate).eventEndTime(eventEndTime)
//				.eventStartTime(eventStartTime).location(location).name(name).maxAttendees(maxAttendees)
//				.ticketCost(ticketCost).build());
//		eventCommands.add(EventCommand.builder().id(id2).build());
//		eventCommands.add(EventCommand.builder().id(id3).build());
//		values.add("eventDate", eventDate);
//		values.add("eventEndTime", eventEndTime);
//		values.add("eventStartTime", eventStartTime);
//		values.add("location", location);
//		values.add("name", name);
//		values.add("maxAttendees", maxAttendees.toString());
//		values.add("ticketCost", ticketCost.toString());
//	}
//
//	@Test
//	void testGetEventList() throws Exception {
//		// given
//		when(service.findAll()).thenReturn(eventCommands);
//
//		// when
//		mockMvc.perform(get("/event/list")).andExpect(status().isOk()).andExpect(view().name("/event/event-list"))
//				.andExpect(model().attribute("events", hasSize(3))).andExpect(model().attribute("active", "event"));
//		// then
//		verify(service, times(1)).findAll();
//	}
//
//	@Test
//	void testAddEvent() throws Exception {
//		mockMvc.perform(get("/event/add")).andExpect(status().isOk()).andExpect(view().name("/event/event-form"))
//				.andExpect(model().attribute("action", ControllerActions.ADD))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV))
//				.andExpect(model().attribute("eventCommand", isA(EventCommand.class)));
//	}
//
//	@Test
//	void testSaveUpdateEvent() throws Exception {
//		// when
//		org.mockito.Mockito.when(service.save(any(EventCommand.class))).thenReturn(eventCommands.get(0));
//
//		// then
//		mockMvc.perform(post("/event/save").contentType(MediaType.APPLICATION_FORM_URLENCODED).params(values))
//				.andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/event/" + id + "/view"))
//				.andExpect(model().attribute("eventCommand", org.hamcrest.Matchers.isA(EventCommand.class)));
//		verify(service, times(1)).save(any(EventCommand.class));
//	}
//
//	@Test
//	void testEditEvent() throws Exception {
//		// when
//		when(service.findById(anyLong())).thenReturn(eventCommands.get(0));
//
//		// then
//		mockMvc.perform(get("/event/" + id + "/edit")).andExpect(view().name("/event/event-form"))
//				.andExpect(status().is2xxSuccessful())
//				.andExpect(model().attribute("eventCommand", org.hamcrest.Matchers.isA(EventCommand.class)))
//				.andExpect(model().attribute("action", ControllerActions.EDIT))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV));
//		verify(service, times(1)).findById(anyLong());
//	}
//
//	@Test
//	void testDeleteEvent() throws Exception {
//		mockMvc.perform(get("/event/" + id + "/delete")).andExpect(view().name("redirect:/event/list"))
//				.andExpect(status().is3xxRedirection());
//	}
//
//	@Test
//	void testViewEvent() throws Exception {
//		// when
//		when(service.findById(anyLong())).thenReturn(eventCommands.get(0));
//
//		// then
//		mockMvc.perform(get("/event/" + id + "/view")).andExpect(view().name("/event/event-form"))
//				.andExpect(status().is2xxSuccessful())
//				.andExpect(model().attribute("eventCommand", org.hamcrest.Matchers.isA(EventCommand.class)))
//				.andExpect(model().attribute("action", ControllerActions.VIEW))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV));
//		verify(service, times(1)).findById(anyLong());
//	}
//
//}

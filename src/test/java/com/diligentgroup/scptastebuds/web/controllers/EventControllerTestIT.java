//package com.diligentgroup.scptastebuds.web.controllers;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithUserDetails;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//class EventControllerTestIT {
//
//	@Autowired
//	WebApplicationContext wac;
//
//	MockMvc mockMVC;
//
//	@BeforeEach
//	void setUp() throws Exception {
//		mockMVC = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
//	}
//
//	@Test
//	void getEventListADMIN() throws Exception {
//		mockMVC.perform(get("/event/list").with(httpBasic("admin", "admin"))).andExpect(status().isOk());
//	}
//
//	@Test
//	void getEventListCFO() throws Exception {
//		mockMVC.perform(get("/event/list").with(httpBasic("cfo", "cfo"))).andExpect(status().isOk());
//	}
//
//	@Test
//	void getEventListBDMBR() throws Exception {
//		mockMVC.perform(get("/event/list").with(httpBasic("bdmbr", "bdmbr"))).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "TBUserDetailsService")
//	void addEventADMIN() throws Exception {
//		mockMVC.perform(get("/event/add").requestAttr("activeYear", anyInt())).andExpect(status().isOk());
//	}
//
//	@Test
//	@WithUserDetails(value = "cfo", userDetailsServiceBeanName = "TBUserDetailsService")
//	void addEventCFO() throws Exception {
//		mockMVC.perform(get("/event/add").requestAttr("activeYear", anyInt())).andExpect(status().isForbidden());
//	}
//
//	@Test
//	void addEventDBMBR() throws Exception {
//		mockMVC.perform(get("/event/add").requestAttr("activeYear", anyInt()).with(httpBasic("bdmbr", "bdmbr"))).andExpect(status().isForbidden());
//	}
//}

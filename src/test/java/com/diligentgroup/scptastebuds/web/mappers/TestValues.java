package com.diligentgroup.scptastebuds.web.mappers;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;

public abstract class TestValues {

	public final static Long id = 1L;
	public final static Long id2 = 2L;
	public final static Long id3 = 3L;
	public final static Long version = 1L; 
	public final static Integer year = 2020;
	public final static String password = "S3cr3t1234";
	public final static String confirmPassword = "confirmS3cr3t";
	public final static String email = "johndoe@johndoe.com";
	public final static String firstName = "John";
	public final static String lastName = "Doe";
	public final static String phone = "123-45-7890";
	public final static String name = "Troublesome CreAk Bandstand";
	public final static String title = "BandStand";
	public final static String url = "www.google.com";
	public final static String location = "Amenity Center";
	public final static AnnualMembershipStatus memberStatus = AnnualMembershipStatus.PAID;
	public final static LocalDate renewalDate = LocalDate.of(2019, 01, 01);
	public final static String renewalDateString = renewalDate.toString();
	public final static String eventDate = LocalDate.of(2019, 12, 12).toString();
	public final static String createdDateTime = LocalDateTime.of(2019, 02, 01, 12, 0).toString();
	public final static String lastUpdatedDateTime = LocalDateTime.of(2018, 03, 01, 11, 30).toString();
	public final static String eventStartTime = LocalTime.of(18, 30).toString();
	public final static String eventEndTime = LocalTime.of(21, 30).toString();
	public final static Integer maxAttendees = Integer.valueOf(45);
	public final static BigDecimal ticketCost = new BigDecimal(5, new MathContext(2));
	public final static String payment = "Chk# 1234";

}

package com.diligentgroup.scptastebuds.web.mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.diligentgroup.scptastebuds.commands.AnnualMembershipCommand;
import com.diligentgroup.scptastebuds.commands.MemberCommand;
import com.diligentgroup.scptastebuds.commands.YearCommand;
import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.Year;

class MemberAndMemberCommandMapperTest extends TestValues {

	DomainCommandMapper mapper;

	@BeforeEach
	void setUp() {
		mapper = new DomainCommandMapperImpl();
	}

	private MemberCommand buildMemberCommand() {
		return MemberCommand.builder().email(TestValues.email).firstName(TestValues.firstName)
				.lastName(TestValues.lastName).id(TestValues.id).phone(TestValues.phone)
				.version(TestValues.version).build();
	}

	private Member buildMember() {
		return Member.builder().email(TestValues.email).firstName(TestValues.firstName).lastName(TestValues.lastName)
				.id(TestValues.id).phone(TestValues.phone).version(TestValues.version)
				.build();
	}

	private Year buildYear() {
		return Year.builder().version(TestValues.version).id(TestValues.year).build();
	}

	private YearCommand buildYearCommand() {
		return YearCommand.builder().version(TestValues.version).id(TestValues.year).build();
	}

	private AnnualMembership buildAnnualMembership() {
		return AnnualMembership.builder().id(TestValues.id).renewalDate(TestValues.renewalDate)
				.version(TestValues.version).payment(TestValues.payment).member(buildMember()).year(buildYear())
				.build();
	}

	@Test
	void memberToMemberCommandTest() {
		MemberCommand command = mapper.memberToMemberCommand(buildMember());
		assertEquals(TestValues.email, command.getEmail());
		assertEquals(TestValues.firstName, command.getFirstName());
		assertEquals(TestValues.lastName, command.getLastName());
		assertEquals(TestValues.id, command.getId());
//		assertEquals(TestValues.password, command.getPassword());
		assertEquals(TestValues.phone, command.getPhone());
		assertEquals(TestValues.version, command.getVersion());
	}

	@Test
	void memberCommandToMemberTest() {
		Member entity = mapper.memberCommandToMember(buildMemberCommand());
		assertEquals(TestValues.email, entity.getEmail());
		assertEquals(TestValues.firstName, entity.getFirstName());
		assertEquals(TestValues.lastName, entity.getLastName());
		assertEquals(TestValues.id, entity.getId());
		//assertEquals(TestValues.password, entity.getPassword());
		assertEquals(TestValues.phone, entity.getPhone());
		assertEquals(TestValues.version, entity.getVersion());
	}

	@Test
	void yearToYearCommandTest() {
		YearCommand command = mapper.yearToYearCommand(buildYear());
		assertEquals(TestValues.year, command.getId());
		assertEquals(TestValues.version, command.getVersion());
	}

	@Test
	void testYearCommandToYear() {
		Year entity = mapper.yearCommandToYear(buildYearCommand());
		assertEquals(TestValues.year, entity.getId());
		assertEquals(TestValues.version, entity.getVersion());
	}

	@Test
	void annualMembershipToAnnualMembershipCommandTest() {
		AnnualMembership annualMembership = buildAnnualMembership();
		AnnualMembershipCommand command = mapper.annualMembershipToAnnualMembershipCommand(annualMembership);
		assertEquals(TestValues.id, command.getId());
		assertEquals(TestValues.version, command.getVersion());
		assertEquals(TestValues.renewalDate, command.getRenewalDate());
		assertEquals(TestValues.payment, command.getPayment());

		if (command.getMemberCommand() != null) {
			assertEquals(TestValues.id, command.getMemberCommand().getId());
			assertEquals(TestValues.email, command.getMemberCommand().getEmail());
			assertEquals(TestValues.firstName, command.getMemberCommand().getFirstName());
			assertEquals(TestValues.lastName, command.getMemberCommand().getLastName());
//			assertEquals(TestValues.password, command.getMemberCommand().getPassword());
			assertEquals(TestValues.phone, command.getMemberCommand().getPhone());
			assertEquals(TestValues.version, command.getMemberCommand().getVersion());
		}
		if (command.getYearCommand() != null) {
			assertEquals(TestValues.year, command.getYearCommand().getId());
			assertEquals(TestValues.version, command.getYearCommand().getVersion());
		}
		System.out.println(annualMembership.toString());
		System.out.println(command.toString());
	}
}

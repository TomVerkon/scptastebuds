//package com.diligentgroup.scptastebuds.controllers;
//
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.List;
//
//import org.hamcrest.Matchers;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.FilterType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.diligentgroup.scptastebuds.commands.EventCommand;
//import com.diligentgroup.scptastebuds.commands.YearCommand;
//import com.diligentgroup.scptastebuds.domain.Event;
//import com.diligentgroup.scptastebuds.domain.EventStatus;
//import com.diligentgroup.scptastebuds.domain.SelectedYearId;
//import com.diligentgroup.scptastebuds.domain.Year;
//import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;
//import com.diligentgroup.scptastebuds.repositories.projections.EventProjection;
//import com.diligentgroup.scptastebuds.services.AnnualMembershipService;
//import com.diligentgroup.scptastebuds.services.EventAttendeeService;
//import com.diligentgroup.scptastebuds.services.EventService;
//import com.diligentgroup.scptastebuds.services.YearService;
//import com.diligentgroup.scptastebuds.web.controllers.ConcreteEventProjection;
//import com.diligentgroup.scptastebuds.web.controllers.ControllerActions;
//import com.diligentgroup.scptastebuds.web.controllers.EventController;
//import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;
//
//@WebMvcTest(controllers = EventController.class, includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = DomainCommandMapper.class))
//class EventControllerTests {
//
//	@Autowired
//	private MockMvc mockMvc;
//
//	// private ActiveYearThreadLocal activeYearThreadLocal;
//
////	@MockBean
////	private DomainCommandMapper mapper;
//	@MockBean
//	private EventService eventService;
//	@MockBean
//	private EventAttendeeService eventAttendeeService;
//	@MockBean
//	private AnnualMembershipService annualMembershipService;
//	@MockBean
//	SelectedYearId selectedYear;
//	@MockBean
//	YearService yearService;
//
//	EventCommand eventCommand;
//
//	Event event;
//
//	YearCommand yearCommand;
//
//	Year year;
//
//	Integer yearId;
//
//	List<EventProjection> eventProjections;
//
//	@BeforeEach
//	void setUp() throws Exception {
//		yearId = Calendar.getInstance().get(Calendar.YEAR);
////		activeYearThreadLocal.set(yearId);
//		yearCommand = YearCommand.builder().id(yearId).build();
//		year = Year.builder().id(yearId).build();
//		selectedYear.setYearId(yearId);
//		eventCommand = EventCommand.builder().yearCommand(YearCommand.builder().id(yearId).build()).build();
//		event = Event.builder().eventDate("2020-10-01").eventEndTime("10:00 PM").eventStartTime("11:00 PM")
//				.eventStatus(EventStatus.ACTIVE).id(1L).location("my home").maxAttendees(55).name("My Event")
//				.ticketCost(new BigDecimal("10.00")).version(0L).year(year).build();
//	}
//
//	@Test
//	public void getEventListRequestTest() throws Exception {
//		eventProjections = new ArrayList<>(Arrays.asList(new ConcreteEventProjection(), new ConcreteEventProjection()));
//		
//		when(eventService.findAllProjectionsByYearId(yearId, null)).thenReturn(eventProjections);
//		
//		mockMvc.perform(get("/event/list")).andExpect(model().attributeExists("eventProjections"))
//		.andExpect(model().attribute("eventProjections", Matchers.iterableWithSize(2)))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV))
//				.andExpect(view().name("/event/event-list")).andExpect(status().isOk());
//	}
//
//	@Test
//	public void viewEventRequestTest() throws Exception {
//
//		when(eventService.findById(anyLong())).thenReturn(event);
//		mockMvc.perform(get("/event/{eventId}/view", 1L)).andExpect(model().attributeExists("eventCommand"))
//				.andExpect(model().attribute("action", ControllerActions.VIEW))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV))
//				.andExpect(view().name("/event/event-form")).andExpect(status().isOk());
//
//	}
//
//	@Test
//	public void addEventVerifyRequestTest() throws Exception {
////		when(mapper.yearToYearCommand(any(Year.class))).thenReturn(yearCommand);
//		when(selectedYear.getYearId()).thenReturn(yearId);
//
//		mockMvc.perform(get("/event/add")).andExpect(model().attributeExists("eventCommand"))
//				.andExpect(model().attribute("action", ControllerActions.ADD))
//				.andExpect(model().attribute("active", EventController.ACTIVE_NAV))
//				.andExpect(view().name("/event/event-form")).andExpect(status().isOk());
////		ArgumentCaptor<EventCommand> eventCommandCaptor = ArgumentCaptor.forClass(EventCommand.class);
//
//	}
//
////	@Test
////	public void saveUpdateEventRequestTest() throws Exception {
////		/*
////		 * @NonNull private YearCommand yearCommand;
////		 * 
////		 * @NotBlank private String name; private String url;
////		 * 
////		 * @NotBlank private String location;
////		 * 
////		 * @NotBlank private String eventDate;
////		 * 
////		 * @NotBlank private String eventStartTime;
////		 * 
////		 * @NotBlank private String eventEndTime;
////		 * 
////		 * @Builder.Default private EventStatus eventStatus = EventStatus.PENDING;
////		 * 
////		 * @NotNull private Integer maxAttendees;
////		 * 
////		 * @NotNull private BigDecimal ticketCost;
////		 * 
////		 */
////		mockMvc.perform(post("/event/save")
////				.param("yearCommand.id", "2020").param("name", "").param("location", "").param("eventDate", "")
////				.param("eventStartTime", "").param("eventEndTime", "").param("eventStatus", "UNPAID")
////	}				.param("maxAttendees", "").param("ticketCost", "").andExpect(status().is4xxClientError());
//
//
//}

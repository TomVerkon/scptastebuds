package com.diligentgroup.scptastebuds.commands;

import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class NewEventAttendeeCommand {

	@NonNull
	@NotNull
	private Long annualMembershipId;

	@NonNull
	@NotNull
	private Long eventId;

	@NonNull
	@NotNull
	private Integer yearId;

	private String eventName;

	@NonNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private AttendeeStatus attendeeStatus = AttendeeStatus.UNPAID;

	List<AnnualMembershipProjection> eligibleMembers;

}

package com.diligentgroup.scptastebuds.commands;

import java.time.LocalDate;

import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AnnualMembershipListCommand extends BaseCommand<Long> {

	@Builder.Default
	private AnnualMembershipStatus annualMembershipStatus = AnnualMembershipStatus.UNPAID;
	private LocalDate renewalDate;
	private String payment;

	private MemberCommand memberCommand;

	private YearCommand yearCommand;

}

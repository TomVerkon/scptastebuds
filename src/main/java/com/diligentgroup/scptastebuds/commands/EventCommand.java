package com.diligentgroup.scptastebuds.commands;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.diligentgroup.scptastebuds.customvalidators.EventDatesMatchConstraint;
import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
import com.diligentgroup.scptastebuds.domain.EventStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@EventDatesMatchConstraint.List({
		@EventDatesMatchConstraint(field = "eventDate", fieldMatch = "yearCommand.id", message = "Event year must equal selected year!") })
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EventCommand extends BaseCommand<Long> {

	@NonNull
	private YearCommand yearCommand;

	@NotBlank
	private String name;
	private String url;
	@NotBlank
	private String location;
	@NotBlank
	private String eventDate;
	@NotBlank
	private String eventStartTime;
	@NotBlank
	private String eventEndTime;
	@Builder.Default
	private EventStatus eventStatus = EventStatus.PENDING;
	@NotNull
	private Integer maxAttendees;
	@NotNull
	private BigDecimal ticketCost;
	@Singular
	@EqualsAndHashCode.Exclude
	private List<EventAttendeeCommand> eventAttendeeCommands;

//	public EventCommand(Long id, String name, String url, String location, String eventDate, String eventStartTime,
//			String eventEndTime, EventStatus eventStatus, Integer maxAttendees, BigDecimal ticketCost) {
//		super(id);
//		this.name = name;
//		this.url = url;
//		this.location = location;
//		this.eventDate = eventDate;
//		this.eventStartTime = eventStartTime;
//		this.eventEndTime = eventEndTime;
//		this.eventStatus = eventStatus;
//		this.maxAttendees = maxAttendees;
//		this.ticketCost = ticketCost;
//	}

//	public List<EventAttendeeCommand> getEventAttendees() {
//		if (null == eventAttendees) {
//			eventAttendees = new ArrayList<>();
//		}
//		return eventAttendees;
//	}
//
//	public void addEventAttendee(EventAttendeeCommand eventAttendeeCommand) {
//		getEventAttendees().add(eventAttendeeCommand);
//	}
//
	public long getNumberOfAttendees() {
		if (getEventAttendeeCommands() != null) {
			return getEventAttendeeCommands().stream()
					.filter(attendee -> attendee.getAttendeeStatus() != AttendeeStatus.DELETED).count();
		} else {
			return Long.parseLong("0");
		}
	}

//	public List<EventAttendeeCommand> getSortedAttendees() {
//		return getEventAttendeeCommands().stream().sorted().collect(Collectors.toList());
//	}
}

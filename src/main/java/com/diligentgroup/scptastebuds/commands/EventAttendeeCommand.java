package com.diligentgroup.scptastebuds.commands;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.diligentgroup.scptastebuds.domain.AttendeeStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EventAttendeeCommand extends BaseCommand<Long> {

	@NonNull
	@NotNull
	private AnnualMembershipCommand annualMembershipCommand;

	@NonNull
	@NotNull
	private EventCommand eventCommand;

	@NonNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private AttendeeStatus attendeeStatus = AttendeeStatus.UNPAID;

}

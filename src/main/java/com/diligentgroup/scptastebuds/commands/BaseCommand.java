package com.diligentgroup.scptastebuds.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@ToString
public abstract class BaseCommand<ID> {

	private ID id;

	private Long version;

	public boolean isNew() {
		return id == null;
	}
}

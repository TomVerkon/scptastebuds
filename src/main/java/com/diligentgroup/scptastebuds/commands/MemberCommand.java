package com.diligentgroup.scptastebuds.commands;

import com.diligentgroup.scptastebuds.domain.MemberStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class MemberCommand extends BaseCommand<Long> {

	private String email;
//	private String password;
//	private String confirmPassword;
	private String firstName;
	private String lastName;
	private String phone;
	@Builder.Default
	private MemberStatus memberStatus = MemberStatus.INACTIVE;

	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private AnnualMembershipCommand annualMembershipCommand;

	public String getFullNameFirstNameFirst() {
		return firstName + " " + lastName;
	}

	public String getFullNameLastNameFirst() {
		return lastName + ", " + firstName;
	}

}

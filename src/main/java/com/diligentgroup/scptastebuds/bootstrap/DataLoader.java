package com.diligentgroup.scptastebuds.bootstrap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.domain.EventStatus;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.MemberStatus;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.domain.security.Authority;
import com.diligentgroup.scptastebuds.domain.security.Role;
import com.diligentgroup.scptastebuds.domain.security.User;
import com.diligentgroup.scptastebuds.repositories.security.AuthorityRepository;
import com.diligentgroup.scptastebuds.repositories.security.RoleRepository;
import com.diligentgroup.scptastebuds.repositories.security.UserRepository;
import com.diligentgroup.scptastebuds.services.AnnualMembershipService;
import com.diligentgroup.scptastebuds.services.EventAttendeeService;
import com.diligentgroup.scptastebuds.services.EventService;
import com.diligentgroup.scptastebuds.services.MemberService;
import com.diligentgroup.scptastebuds.services.YearService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

	private final MemberService memberService;
	private final EventService eventService;
	private final YearService yearService;
	private final AnnualMembershipService annualMembershipService;
	private final EventAttendeeService eventAttendeeService;
	private final AuthorityRepository authorityRepo;
	private final UserRepository userRepo;
	private final PasswordEncoder passwordEncoder;
	private final RoleRepository roleRepository;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("Entering DataLoader");
		if (memberService.getCount() == 0) {

			loadYears();
			Year activeYear = yearService.findById(2020);
			ActiveYearThreadLocal.set(activeYear.getId());

			Map<String, String> phoneNumberMap = loadPhoneNumbersByName();

			loadUsers();

			loadFromFile(phoneNumberMap);

			List<Event> events = new ArrayList<>();
			events.add(Event.builder().name("Taste of Louisiana Crawfish Low Country Boil").eventDate("2020-03-21")
					.eventEndTime("9:00 PM").eventStartTime("5:00 PM").location("Amenity Center").maxAttendees(50)
					.ticketCost(new BigDecimal("5.25", MathContext.DECIMAL32)).year(activeYear)
					.eventStatus(EventStatus.ACTIVE).build());
			events.add(Event.builder().name("The Hungry Italian").eventDate("2020-04-21").eventEndTime("9:00 PM")
					.eventStartTime("5:00 PM").location("Amenity Center").maxAttendees(40)
					.ticketCost(new BigDecimal("6.00", MathContext.DECIMAL32)).year(activeYear)
					.eventStatus(EventStatus.ACTIVE).build());
			events.add(Event.builder().name("Pittypat's Porch").eventDate("2020-05-21").eventEndTime("9:00 PM")
					.eventStartTime("5:00 PM").location("Amenity Center").maxAttendees(30)
					.ticketCost(new BigDecimal("7.00", MathContext.DECIMAL32)).year(activeYear)
					.eventStatus(EventStatus.ACTIVE).build());
			events.add(Event.builder().name("Low Country Boil").eventDate("2020-06-21").eventEndTime("9:00 PM")
					.eventStartTime("5:00 PM").location("Amenity Center").maxAttendees(20)
					.ticketCost(new BigDecimal("25.25", MathContext.DECIMAL32)).year(activeYear)
					.eventStatus(EventStatus.ACTIVE).build());
			events.add(Event.builder().name("Everything Dumplings").eventDate("2019-07-21").eventEndTime("9:00 PM")
					.eventStartTime("5:00 PM").location("Sales Center").maxAttendees(10)
					.ticketCost(new BigDecimal("105.55", MathContext.DECIMAL32)).year(yearService.findById(2019))
					.eventStatus(EventStatus.CLOSED).build());

			events = eventService.saveAll(events);

			List<AnnualMembership> annualMemberships = new ArrayList<>();
			annualMemberships = annualMembershipService.findByYearAndAnnualMembershipStatus(activeYear,
					AnnualMembershipStatus.PAID);

			for (Event currentEvent : events) {
				int i = 0;
				if (!currentEvent.getYear().equals(activeYear)) {
					continue;
				}
				for (AnnualMembership annualMembership : annualMemberships) {
					EventAttendee eventAttendee = EventAttendee.builder().annualMembership(annualMembership)
							.event(currentEvent).build();
					if (i < 15) {
						eventAttendee.setAttendeeStatus(AttendeeStatus.PAID);
					}
					eventAttendeeService.save(eventAttendee);
					i++;
					if (i > 30)
						break;
				}
			}
		}
	}

	public void loadYears() {

		List<Year> years = new ArrayList<>();

		years.add(Year.builder().id(2019).build());
		years.add(Year.builder().id(2020).build());
		years.add(Year.builder().id(2021).build());
		yearService.saveAll(years);
		yearService.save(Year.builder().id(2022).build());
	}

	@Transactional
	public void loadUsers() {

		// member authorities
		Authority createMember = authorityRepo.save(Authority.builder().permission("member.create").build());
		Authority readMember = authorityRepo.save(Authority.builder().permission("member.read").build());
		Authority updateMember = authorityRepo.save(Authority.builder().permission("member.update").build());
		Authority deleteMember = authorityRepo.save(Authority.builder().permission("member.delete").build());
		Authority reportMember = authorityRepo.save(Authority.builder().permission("member.report").build());

		// membership authorities
		Authority createMembership = authorityRepo.save(Authority.builder().permission("membership.create").build());
		Authority readMembership = authorityRepo.save(Authority.builder().permission("membership.read").build());
		Authority updateMembership = authorityRepo.save(Authority.builder().permission("membership.update").build());
		Authority deleteMembership = authorityRepo.save(Authority.builder().permission("membership.delete").build());
		Authority reportMembership = authorityRepo.save(Authority.builder().permission("membership.report").build());
		Authority emailMembership = authorityRepo.save(Authority.builder().permission("membership.email").build());

		// Event authorities
		Authority createEvent = authorityRepo.save(Authority.builder().permission("event.create").build());
		Authority readEvent = authorityRepo.save(Authority.builder().permission("event.read").build());
		Authority updateEvent = authorityRepo.save(Authority.builder().permission("event.update").build());
		Authority deleteEvent = authorityRepo.save(Authority.builder().permission("event.delete").build());
		Authority reportEvent = authorityRepo.save(Authority.builder().permission("event.report").build());

		// EventAttendee authorities
		Authority createEventAttendee = authorityRepo
				.save(Authority.builder().permission("event.attendee.create").build());
		Authority readEventAttendee = authorityRepo.save(Authority.builder().permission("event.attendee.read").build());
		Authority updateEventAttendee = authorityRepo
				.save(Authority.builder().permission("event.attendee.update").build());
		Authority deleteEventAttendee = authorityRepo
				.save(Authority.builder().permission("event.attendee.delete").build());
		Authority reportEventAttendee = authorityRepo
				.save(Authority.builder().permission("event.attendee.report").build());
		Authority emailEventAttendee = authorityRepo
				.save(Authority.builder().permission("event.attendee.email").build());

		// Year authorities
		Authority createYear = authorityRepo.save(Authority.builder().permission("year.create").build());
		Authority readYear = authorityRepo.save(Authority.builder().permission("year.read").build());
		Authority updateYear = authorityRepo.save(Authority.builder().permission("year.update").build());
		Authority deleteYear = authorityRepo.save(Authority.builder().permission("year.delete").build());

		// Save admin role
		Role adminRole = roleRepository.save(Role.builder().name("ADMIN").build());
		// update admin role with authorities
		adminRole.setAuthorities(Set.of(createMember, readMember, updateMember, deleteMember, reportMember,
				createMembership, readMembership, updateMembership, reportMembership, deleteMembership, createEvent,
				readEvent, updateEvent, deleteEvent, reportEvent, createEventAttendee, readEventAttendee,
				updateEventAttendee, deleteEventAttendee, reportEventAttendee, createYear, readYear, updateYear,
				deleteYear, emailMembership, emailEventAttendee));
		// save updated role
		adminRole = roleRepository.save(adminRole);

		Role cfoRole = roleRepository.save(Role.builder().name("CFO").build());
		cfoRole.setAuthorities(Set.of(readMember, reportMember, createMembership, readMembership, updateMembership,
				reportMembership, readEvent, reportEvent, createEventAttendee, readEventAttendee, updateEventAttendee,
				deleteEventAttendee, reportEventAttendee, readYear, emailMembership, emailEventAttendee));
		roleRepository.save(cfoRole);

		Role bdmbrRole = roleRepository.save(Role.builder().name("BDMBR").build());
		bdmbrRole.setAuthorities(Set.of(readMember, reportMember, readMembership, reportMembership, readEvent,
				reportEvent, readEventAttendee, reportEventAttendee, readYear));
		roleRepository.save(bdmbrRole);


		User user = userRepo.save(User.builder().username("admin").password(passwordEncoder.encode("admin")).build());
		user.setRoles(Set.of(adminRole));
		userRepo.save(user);
		
		user = userRepo
				.save(User.builder().username("tverkon@gmail.com").password(passwordEncoder.encode("TroublesomeCreAk")).build());
		user.setRoles(Set.of(adminRole));
		userRepo.save(user);

		user = userRepo
				.save(User.builder().username("dverkon@gmail.com").password(passwordEncoder.encode("CupCake")).build());
		user.setRoles(Set.of(adminRole));
		userRepo.save(user);
		
		user = userRepo.save(User.builder().username("barfurr@aol.com").password(passwordEncoder.encode("Sherbert"))
				.build());
		user.setRoles(Set.of(adminRole));
		userRepo.save(user);

		user = userRepo.save(User.builder().username("malonejoyce@hotmail.com")
				.password(passwordEncoder.encode("ApplePie")).build());
		user.setRoles(Set.of(cfoRole));
		userRepo.save(user);

		user = userRepo.save(User.builder().username("cfo").password(passwordEncoder.encode("cfo")).build());
		user.setRoles(Set.of(cfoRole));
		userRepo.save(user);

		user = userRepo.save(
				User.builder().username("jrp410s@yahoo.com").password(passwordEncoder.encode("JackDaniels")).build());
		user.setRoles(Set.of(bdmbrRole));
		userRepo.save(user);

		user = userRepo.save(
				User.builder().username("bdmbr").password(passwordEncoder.encode("bdmbr")).build());
		user.setRoles(Set.of(bdmbrRole));
		user = userRepo.save(user);
	}

	public void loadStaticData() {

	}

	public Map<String, String> loadPhoneNumbersByName() {
		Map<String, String> phoneNumberMap = new HashMap<String, String>();
		try {
			InputStream resource = new ClassPathResource("residents.csv").getInputStream();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
				String line = reader.readLine();
				while (line != null) {
					String name = null;
					Pattern p = Pattern.compile("\"([^\"]*)\"");
					Matcher m = p.matcher(line);
					while (m.find()) {
						name = m.group(1);
						// log.debug(m.group(1));
					}
					String phone = null;
					String[] elements = line.split(",");
					log.debug(String.valueOf(elements.length));
					if (elements.length == 3) {
						String tempPhone = elements[2];
						if (tempPhone.length() == 10) {
							phone = tempPhone.substring(0, 3) + "-" + tempPhone.substring(3, 6) + "-"
									+ tempPhone.substring(6);
						} else if (tempPhone.length() == 12 && tempPhone.charAt(3) == '-'
								&& tempPhone.charAt(7) == '-') {
							phone = tempPhone;
						}
						if (name != null && phone != null) {
							log.debug(name + "  " + phone);
							phoneNumberMap.put(name, phone);
						}
					}
//						log.debug(elements[0]);
//					}
					line = reader.readLine();
				}
			} catch (Exception e) {
				e.printStackTrace();

			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return phoneNumberMap;
	}

	public void loadFromFile(Map<String, String> phoneNumberMap) {

		Integer activeYearId = ActiveYearThreadLocal.get();
		Year activeYear = yearService.findById(activeYearId);

		try {
			InputStream resource = new ClassPathResource("2020Members.csv").getInputStream();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource))) {
				String line = reader.readLine();
				while (line != null) {
					String[] elements = line.split(",");
					if (StringUtils.isNotBlank(elements[2])) {
						Member member = null;
						String name = elements[0] + ", " + elements[1];
						String phone = phoneNumberMap.get(name);
						if (StringUtils.isBlank(phone)) {
							log.debug("Missing phone: " + name);
						}
						if (StringUtils.isBlank(phone)) {
							member = memberService.save(
									Member.builder().lastName(elements[0]).firstName(elements[1]).email(elements[2])
											.phone("000-000-0000").memberStatus(MemberStatus.ACTIVE).build());
						} else {
							member = memberService.save(Member.builder().lastName(elements[0]).firstName(elements[1])
									.email(elements[2]).phone(phone).memberStatus(MemberStatus.ACTIVE).build());
						}
						if (StringUtils.equals("PAID", elements[3])) {
							AnnualMembership annualMembership = AnnualMembership.builder().member(member)
									.annualMembershipStatus(AnnualMembershipStatus.PAID).year(activeYear).build();
							annualMembershipService.save(annualMembership);
						}
					} else {
						log.debug("Missing email: " + line);
					}
					line = reader.readLine();
				}
			}
			phoneNumberMap.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package com.diligentgroup.scptastebuds.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@RequiredArgsConstructor
@SuperBuilder
@Entity
@Table(name = "event_attendees")
public class EventAttendee extends BaseEntity<Long> {

	@NonNull
	@NotNull
	@ManyToOne
	@JoinColumn(name = "annual_membership_id")
	private AnnualMembership annualMembership;

	@NonNull
	@NotNull
	@ManyToOne
	@JoinColumn(name = "event_id")
	@ToString.Exclude
	private Event event;

	@NonNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private AttendeeStatus attendeeStatus = AttendeeStatus.UNPAID;

//	public EventAttendee(Long id, Long version, Timestamp createdDate, Timestamp lastModifiedDate,
//			AnnualMembership attendee, Boolean paidFlag, AttendeeStatus attendeeStatus) {
//		super(id, version, createdDate, lastModifiedDate);
//		this.attendee = attendee;
//		this.paidFlag = paidFlag;
//		this.attendeeStatus = attendeeStatus;
//	}

}

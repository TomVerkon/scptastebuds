package com.diligentgroup.scptastebuds.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailTarget {
	
	private String firstName;
	private String lastName;
	private String email;
	private String Phone;
	private AnnualMembershipStatus annualMembershipStatus;
	private AttendeeStatus attendeeStatus;
	
	public String getFullNameLastFirst() {
		return this.lastName + ", " + this.firstName;
	}
}

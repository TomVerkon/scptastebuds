package com.diligentgroup.scptastebuds.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Entity()
@Table(name = "members", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "email", "firstName" }, name = "uniqueEmailFnameConstraint") })
public class Member extends BaseEntity<Long> {

	private String email;
	// private String password;
	private String firstName;
	private String lastName;
	private String phone;

	@NonNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private MemberStatus memberStatus = MemberStatus.INACTIVE;

	// @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval =
	// true, fetch = FetchType.LAZY)
	@OneToMany(mappedBy = "member", fetch = FetchType.LAZY)
	@Builder.Default
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private List<AnnualMembership> annualMemberships = new ArrayList<>();

}

package com.diligentgroup.scptastebuds.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@MappedSuperclass
@SuperBuilder
public abstract class AuditedEntity {

	@Version
	private Long version;

	@CreationTimestamp
	@Column(updatable = false)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private Timestamp createdDate;

	@UpdateTimestamp
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private Timestamp lastModifiedDate;

}
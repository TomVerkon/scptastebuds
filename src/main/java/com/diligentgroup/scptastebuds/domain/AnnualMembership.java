package com.diligentgroup.scptastebuds.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Entity()
@Table(name = "annual_memberships")
public class AnnualMembership extends BaseEntity<Long> {

	@NonNull
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "member_id")
	private Member member;

	@NonNull
	@ManyToOne
	@JoinColumn(name = "year_id")
	@ToString.Exclude
	private Year year;

	@OneToMany(mappedBy = "annualMembership", fetch = FetchType.LAZY)
	@Builder.Default
	@ToString.Exclude
	private List<EventAttendee> eventsAttended = new ArrayList<>();

	@NonNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private AnnualMembershipStatus annualMembershipStatus = AnnualMembershipStatus.UNPAID;

	private LocalDate renewalDate;
	private String payment;

}

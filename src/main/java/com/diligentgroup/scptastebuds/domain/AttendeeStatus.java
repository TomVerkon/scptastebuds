package com.diligentgroup.scptastebuds.domain;

public enum AttendeeStatus {
	PAID, UNPAID, DELETED
}

package com.diligentgroup.scptastebuds.domain;

public enum EventStatus {
	PENDING, ACTIVE, CLOSED, DELETED
}

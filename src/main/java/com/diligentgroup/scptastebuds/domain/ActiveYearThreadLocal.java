package com.diligentgroup.scptastebuds.domain;

public class ActiveYearThreadLocal {

	public static final ThreadLocal<Integer> activeYearThreadLocal = new ThreadLocal<Integer>();

	public static void set(Integer yearId) {
		activeYearThreadLocal.set(yearId);
	}

	public static void unset() {
		activeYearThreadLocal.remove();
	}

	public static Integer get() {
		return activeYearThreadLocal.get();
	}
}
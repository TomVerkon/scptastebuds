package com.diligentgroup.scptastebuds.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

import org.springframework.data.domain.Persistable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@MappedSuperclass
@SuperBuilder
public abstract class BaseEntity<ID> extends AuditedEntity implements Persistable<ID> {

//	public BaseEntity(Long id, Long version, Timestamp createdDate, Timestamp lastModifiedDate) {
//		this.id = id;
//		this.version = version;
//		this.createdDate = createdDate;
//		this.lastModifiedDate = lastModifiedDate;
//	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tastebuds_generator")
	@SequenceGenerator(name = "tastebuds_generator", sequenceName = "tastebuds_seq", allocationSize = 50)
	@Column(name = "id", updatable = false, nullable = false)
	private ID id;

	@Override
	public boolean isNew() {
		return id == null;
	}

}
package com.diligentgroup.scptastebuds.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.diligentgroup.scptastebuds.customvalidators.EventDatesMatchConstraint;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@EventDatesMatchConstraint.List({
		@EventDatesMatchConstraint(field = "eventDate", fieldMatch = "year.id", message = "Event year must equal selected year!") })
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "events")
public class Event extends BaseEntity<Long> {

	@OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
	@Singular
	private List<EventAttendee> eventAttendees;

	@ManyToOne
	@JoinColumn(name = "year_id")
	@ToString.Exclude
	private Year year;

	@NotBlank
	private String name;

	@NotBlank
	private String location;

	private String eventDate; // in the format of yyyy-mm-dd

	@NotBlank
	private String eventStartTime; // in the format of HH:mm

	@NotBlank
	private String eventEndTime; // in the format of HH:mm

	@NotNull
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private EventStatus eventStatus = EventStatus.PENDING;

	@NotNull
	private Integer maxAttendees;

	@NotNull
	private BigDecimal ticketCost;

	public boolean isNew() {
		return getId() == null;
	}

}

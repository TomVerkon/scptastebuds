package com.diligentgroup.scptastebuds.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString()
public class SelectedYearId {

	public Integer yearId;

}

package com.diligentgroup.scptastebuds.domain;

public enum MemberStatus {
	ACTIVE, INACTIVE
}

package com.diligentgroup.scptastebuds.domain;

public enum AnnualMembershipStatus {
	PAID, UNPAID
}

package com.diligentgroup.scptastebuds.repositories.security;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diligentgroup.scptastebuds.domain.security.Role;

public interface RoleRepository  extends JpaRepository<Role, Long>{

}

package com.diligentgroup.scptastebuds.repositories.security;

import org.springframework.data.jpa.repository.JpaRepository;

import com.diligentgroup.scptastebuds.domain.security.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long>{

}

package com.diligentgroup.scptastebuds.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventStatus;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.projections.EventProjection;

@Repository
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {

	List<Event> findByYearAndEventStatus(Year year, EventStatus eventStatus);

	/*
	 * Long getEventId();
	 * 
	 * Long getEventVersion();
	 * 
	 * Integer getYearId();
	 * 
	 * String getName();
	 * 
	 * String getEventDate();
	 * 
	 */
	@Query(value = "select e.id as eventId, "
			+ "e.name as name, "
			+ "e.eventDate as eventDate, "
			+ "e.eventStatus as eventStatus, "
			+ "e.maxAttendees as maxAttendees, " 
			+ "(select count(*) from EventAttendee ea1 where e = ea1.event and ea1.attendeeStatus = 'PAID') as ticketsSold, "
			+ "(select count(*) from EventAttendee ea2 where e = ea2.event and ea2.attendeeStatus = 'UNPAID') as ticketsReserved "
			+ "from Event e "
			+ "where e.year.id = :yearId "
			+ "order by e.eventDate asc")
	List<EventProjection> findAllProjectionsByYearId(Integer yearId, Pageable pageable);

	@Query(value = "select e.id as eventId, e.name as name, e.eventDate as eventDate from "
			+ "Event e where e.id = :eventId")
	EventProjection findProjectionById(Long eventId);

}

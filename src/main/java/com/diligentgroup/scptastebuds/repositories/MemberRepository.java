package com.diligentgroup.scptastebuds.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.MemberStatus;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

	public Optional<Member> findByEmailAndFirstName(String email, String firstName);
	
	public List<Member> findAllByMemberStatus(MemberStatus memberStatus);

}

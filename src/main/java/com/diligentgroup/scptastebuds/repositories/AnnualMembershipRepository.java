package com.diligentgroup.scptastebuds.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;

@Repository
public interface AnnualMembershipRepository extends JpaRepository<AnnualMembership, Long> {

	public List<AnnualMembership> findByYearAndAnnualMembershipStatus(Year year,
			AnnualMembershipStatus annualMembershipStatus);

	// public List<AnnualMembership> findByYear(Year year);

	public AnnualMembership findByMemberAndYearId(Member member, Integer yearId);

	@Query(value = "select am.id as annualMembershipId, m.id as memberId, "
			+ "m.last_name as lastName, m.first_name as firstName, " + "m.email as email from	annual_memberships am "
			+ "join members m on am.member_id = m.id where annual_membership_status = 'PAID' "
			+ "and year_id = :yearId and am.id not in (select ea.annual_membership_id from event_attendees ea "
			+ "where ea.event_id = :eventId)", nativeQuery = true)
	public List<AnnualMembershipProjection> getEligibleMembershipsByYearAndEvent(@Param("yearId") Integer yearId,
			@Param("eventId") Long eventId);

//	@Query(value = "select am.id as annualMembershipId, m.id as memberId,"
//			+ "m.last_name as lastName, m.first_name as firstName, "
//			+ "m.email as email, am.annual_membership_status as annualMembershipStatus from "
//			+ "annual_memberships am join members m on am.member_id = m.id where year_id = :yearId", nativeQuery = true)
//	public List<AnnualMembershipProjection> getMembershipProjectionsByYear(@Param("yearId") Integer yearId);

	@Query(value = "select " + 
			"	am.id as annualMembershipId, " + 
			"	m.id as memberId, " + 
			"	m.last_name as lastName, " + 
			"	m.first_name as firstName, " + 
			"	m.email as email, " + 
			"	m.phone as phone, " + 
			"	am.annual_membership_status as annualMembershipStatus " + 
			"from " + 
			"	members m " + 
			"left join annual_memberships am on " + 
			"	m.id = am.member_id " + 
			"	and am.year_id = :yearId " + 
			"where " + 
			"	m.member_status = 'ACTIVE' order by m.last_name asc, m.first_name asc", nativeQuery = true)
	public List<AnnualMembershipProjection> getMembershipProjectionsByYear(@Param("yearId") Integer yearId);
}

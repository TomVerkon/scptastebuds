package com.diligentgroup.scptastebuds.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.diligentgroup.scptastebuds.domain.Year;

@Repository
public interface YearRepository extends JpaRepository<Year, Integer> {

}

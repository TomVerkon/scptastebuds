package com.diligentgroup.scptastebuds.repositories.projections;

import java.util.List;

import com.diligentgroup.scptastebuds.domain.AttendeeStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class EventAttendeeProjectionList {

	private final List<EventAttendeeProjection> projections;

	private Long getCountForStatus(AttendeeStatus status) {
		if (null == projections || projections.isEmpty()) {
			return 0L;
		} else {
			return projections.stream().filter(projection -> projection.getAttendeeStatus() == status).count();
		}
	}

	public Long getSignedUpCount() {
		return getCountForStatus(AttendeeStatus.PAID) + getCountForStatus(AttendeeStatus.UNPAID);
	}

	public Long getPaidCount() {
		return getCountForStatus(AttendeeStatus.PAID);
	}

	public Long getUnpaidCount() {
		return getCountForStatus(AttendeeStatus.UNPAID);
	}

	public Long getDeletedCount() {
		return getCountForStatus(AttendeeStatus.DELETED);
	}

}

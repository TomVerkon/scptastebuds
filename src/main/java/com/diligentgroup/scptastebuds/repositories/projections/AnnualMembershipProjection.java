package com.diligentgroup.scptastebuds.repositories.projections;

import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.domain.MemberStatus;

public interface AnnualMembershipProjection {

	Long getAnnualMembershipId();

	Long getMemberId();

	Integer getYearId();

	String getFirstName();

	String getLastName();

	String getEmail();
	
	String getPhone();
	
	MemberStatus getMemberStatus();

	AnnualMembershipStatus getAnnualMembershipStatus();

}

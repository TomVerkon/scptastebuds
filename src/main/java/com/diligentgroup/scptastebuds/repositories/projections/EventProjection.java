package com.diligentgroup.scptastebuds.repositories.projections;

import com.diligentgroup.scptastebuds.domain.EventStatus;

public interface EventProjection {

	Long getEventId();

	Long getEventVersion();

	Integer getYearId();

	String getName();

	String getEventDate();

	EventStatus getEventStatus();
	
	Integer getMaxAttendees();
	
	Integer getTicketsSold();

	Integer getTicketsReserved();

	EventAttendeeProjectionList getEventAttendeeProjectionList();

}

package com.diligentgroup.scptastebuds.repositories.projections;

import java.util.List;

import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class AnnualMembershipProjectionList {

	private final List<AnnualMembershipProjection> projections;

	private Long getCountForStatus(AnnualMembershipStatus status) {
		if (null == projections || projections.isEmpty()) {
			return 0L;
		} else {
			return projections.stream().filter(projection -> projection.getAnnualMembershipStatus() == status).count();
		}
	}

	public Long getPaidCount() {
		return getCountForStatus(AnnualMembershipStatus.PAID);
	}

	public Long getUnpaidCount() {
		return getCountForStatus(AnnualMembershipStatus.UNPAID);
	}


}

package com.diligentgroup.scptastebuds.repositories.projections;

import com.diligentgroup.scptastebuds.domain.AttendeeStatus;

public interface EventAttendeeProjection {

	Long getEventAttendeeId();

	Long getEventAttendeeVersion();

	Long getAnnualMembershipId();

	String getFirstName();

	String getLastName();

	String getEmail();

	String getPhone();

	AttendeeStatus getAttendeeStatus();

}

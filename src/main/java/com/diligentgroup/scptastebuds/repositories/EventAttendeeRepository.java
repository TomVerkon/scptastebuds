package com.diligentgroup.scptastebuds.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjection;

public interface EventAttendeeRepository extends CrudRepository<EventAttendee, Long> {

	/*
	 * Long getEventAttendeeId(); Long getEventAttendeeVersion(); Long
	 * getAnnualMembershipId(); String getFirstName(); String getLastName();
	 * AttendeeStatus getAttendeeStatus();
	 */
	@Query(value = "select ea.id as eventAttendeeId, ea.version as eventAttendeeVersion, "
			+ "ea.annualMembership.id as annualMembershipId, " + "ea.annualMembership.member.firstName as firstName, "
			+ "ea.annualMembership.member.lastName as lastName, " + "ea.annualMembership.member.email as email, "
			+ "ea.annualMembership.member.phone as phone, "+ "ea.attendeeStatus as attendeeStatus from "
			+ "EventAttendee ea join AnnualMembership am on ea.annualMembership = am "
			+ "join Member m on am.member = m where ea.event.id = :eventId")
	List<EventAttendeeProjection> findByEventId(Long eventId, Pageable pageable);

}

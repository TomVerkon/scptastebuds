package com.diligentgroup.scptastebuds.repositories;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = -8886706832039727740L;

	public NotFoundException(String message) {
		super(message);
	}
}

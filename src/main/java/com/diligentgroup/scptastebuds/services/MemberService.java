package com.diligentgroup.scptastebuds.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;

import com.diligentgroup.scptastebuds.domain.Member;

public interface MemberService {

	public long getCount();

	public List<Member> saveAll(List<Member> members);

	public List<Member> findAll(Pageable pageable);

	public Member findByEmailAndFirstName(String email, String firstName);

	public Member save(Member member);

	public Member findById(Long id);

	public void deleteById(Long id);

	public void deleteAll();

	@Transactional
	public Member encryptPassword(Member member);

}

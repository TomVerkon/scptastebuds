package com.diligentgroup.scptastebuds.services;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventStatus;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.projections.EventProjection;

public interface EventService {

	Event save(Event event);

	List<Event> saveAll(List<Event> events);

	Event findById(Long id);

	Event findEventOnlyById(Long id);

	List<Event> findByYearAndEventStatus(Year year, EventStatus eventStatus);

	List<EventProjection> findAllProjectionsByYearId(Integer yearId, Pageable pageable);

	void deleteById(Long eventId);

	EventProjection findProjectionById(Long eventId);

	ByteArrayInputStream generateEventPDFReport(Long eventId);
}

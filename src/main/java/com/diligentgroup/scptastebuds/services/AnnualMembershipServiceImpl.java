package com.diligentgroup.scptastebuds.services;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.AnnualMembershipRepository;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AnnualMembershipServiceImpl implements AnnualMembershipService {

	private final AnnualMembershipRepository repository;
	private final MemberService memberService;

	@Override
	@Transactional
	public AnnualMembership save(AnnualMembership annualMembership) {
		log.debug("save(AnnualMembership annualMembership:) " + annualMembership.toString());
		if (annualMembership.getMember().isNew()) {
			// encrypt password and persist member
			annualMembership.setMember(memberService.save(annualMembership.getMember()));
		}
		return repository.save(annualMembership);
	}

	@Transactional(value = TxType.SUPPORTS)
	public List<AnnualMembership> findByYearAndAnnualMembershipStatus(Year year,
			AnnualMembershipStatus annualMembershipStatus) {
		return repository.findByYearAndAnnualMembershipStatus(year, annualMembershipStatus);
	}

	@Transactional
	public List<AnnualMembershipProjection> getMembershipProjectionsByYear(Integer yearId, Pageable pageable) {
		List<AnnualMembershipProjection> projections = repository.getMembershipProjectionsByYear(yearId);
		return projections;
	}

	@Override
	@Transactional
	public List<AnnualMembership> saveAll(List<AnnualMembership> annualMemberships) {
		for (AnnualMembership annualMembership : annualMemberships) {
			if (annualMembership.getMember().isNew()) {
				annualMembership.setMember(memberService.save(annualMembership.getMember()));
			}
		}
		return StreamSupport.stream(repository.saveAll(annualMemberships).spliterator(), false)
				.collect(Collectors.toList());
	}

	@Transactional(value = TxType.SUPPORTS)
	public AnnualMembership findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("AnnualMembership with id: " + id.toString()));
	}

	@Override
	@Transactional(value = TxType.SUPPORTS)
	public AnnualMembership findByMemberAndYearId(Member member, Integer yearId) {
		// TODO Auto-generated method stub
		return repository.findByMemberAndYearId(member, yearId);
	}

	@Override
	@Transactional
	public AnnualMembership updateMembershipStatus(Long annualMembershipId,
			AnnualMembershipStatus annualMembershipStatus) {
		AnnualMembership annualMembership = findById(annualMembershipId);
		annualMembership.setAnnualMembershipStatus(annualMembershipStatus);
		return repository.save(annualMembership);
	}

//	@Override
//	@Transactional
//	public List<AnnualMembership> getEligibleMembershipsForEvent(Event event) {
//		List<EventAttendee> currentAttendees = eaRepository.findByEvent(event);
//		Set<Long> currentAttendeeMap = new HashSet<Long>();
//		for (EventAttendee eventAttendee : currentAttendees) {
//			currentAttendeeMap.add(eventAttendee.getAnnualMembership().getId());
//		}
//		List<AnnualMembership> eligibleannualMembers = repository
//				.findByYearAndAnnualMembershipStatus(event.getYear(), AnnualMembershipStatus.PAID).stream()
//				.filter(annualMembership -> !currentAttendeeMap.contains(annualMembership.getId()))
//				.collect(Collectors.toList());
//		return eligibleannualMembers;
//	}

	@Override
	@Transactional
	public List<AnnualMembershipProjection> getEligibleMembershipsByYearAndEvent(Integer yearId, Long eventId) {
		return repository.getEligibleMembershipsByYearAndEvent(yearId, eventId);
	}

	@Override
	public List<AnnualMembership> getEligibleMembershipsForEvent(Event event) {
		// TODO Auto-generated method stub
		return null;
	}

}

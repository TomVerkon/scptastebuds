package com.diligentgroup.scptastebuds.services;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.repositories.AnnualMembershipRepository;
import com.diligentgroup.scptastebuds.repositories.MemberRepository;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

//	private PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

	private final MemberRepository repository;
	private final AnnualMembershipRepository membershipRepository;
	private final YearService yearService;

	public long getCount() {
		return repository.count();
	}

	@Transactional(value = TxType.SUPPORTS)
	public List<Member> findAll(Pageable pageable) {
		return StreamSupport.stream(repository.findAll(pageable).spliterator(), false).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Member save(Member member) {
		log.debug("save(Member member)");
		boolean newMember = member.isNew();
		member = repository.save(member);
//		if (newMember) {
//			AnnualMembership annualMembership = AnnualMembership.builder()
//					.annualMembershipStatus(AnnualMembershipStatus.UNPAID).member(member)
//					.year(yearService.findById(ActiveYearThreadLocal.get())).build();
//			membershipRepository.save(annualMembership);
//		}
		return member;
	}

	@Override
	public Member findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new NotFoundException("Member with id: " + id.toString()));
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	@Transactional
	public List<Member> saveAll(List<Member> members) {
		/*
		 * return StreamSupport.stream(repository.saveAll(members.stream().map(this::
		 * encryptPassword).collect(Collectors.toList())).spliterator(),
		 * false).collect(Collectors.toList());
		 */
		return StreamSupport.stream(repository.saveAll(members).spliterator(), false).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void deleteAll() {
		repository.deleteAll();
	}

	@Transactional
	public Member encryptPassword(Member member) {
		if (member.isNew()) {
			// member.setPassword(encoder.encode(member.getPassword()));
		}
		return member;
	}

	@Override
	public Member findByEmailAndFirstName(String email, String firstName) {
		return repository.findByEmailAndFirstName(email, firstName).orElseThrow(
				() -> new NotFoundException("Member with email: " + email + " and firstName: " + firstName));
	}

}

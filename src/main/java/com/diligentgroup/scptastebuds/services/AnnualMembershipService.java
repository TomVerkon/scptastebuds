package com.diligentgroup.scptastebuds.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;

public interface AnnualMembershipService {

	AnnualMembership save(AnnualMembership annualMembership);

	List<AnnualMembership> saveAll(List<AnnualMembership> annualMemberships);

	AnnualMembership findById(Long id);

	List<AnnualMembership> findByYearAndAnnualMembershipStatus(Year year,
			AnnualMembershipStatus annualMembershipStatus);

	List<AnnualMembershipProjection> getMembershipProjectionsByYear(Integer yearId, Pageable pageable);

	AnnualMembership findByMemberAndYearId(Member member, Integer yearId);

	AnnualMembership updateMembershipStatus(Long annualMembershipId, AnnualMembershipStatus annualMembershipStatus);

	List<AnnualMembership> getEligibleMembershipsForEvent(Event event);

	List<AnnualMembershipProjection> getEligibleMembershipsByYearAndEvent(Integer yearId, Long eventId);
}

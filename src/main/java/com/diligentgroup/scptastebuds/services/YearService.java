package com.diligentgroup.scptastebuds.services;

import java.util.List;

import org.springframework.data.domain.Sort;

import com.diligentgroup.scptastebuds.domain.Year;

public interface YearService {

	List<Year> saveAll(List<Year> years);

	List<Year> findAll(Sort sort);

	Year save(Year year);

	Year findById(Integer id);

//	Year updateSelectedYear(Year year);

}

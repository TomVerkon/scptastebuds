package com.diligentgroup.scptastebuds.services;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.repositories.YearRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class YearServiceImpl implements YearService {

	private final YearRepository yearRepository;

	@Override
	public List<Year> saveAll(List<Year> years) {
		// return StreamSupport.stream(yearRepository.saveAll(years).spliterator(),
		// false).collect(Collectors.toList());
		return yearRepository.saveAll(years);
	}

	@Override
	public List<Year> findAll(Sort sort) {
		return yearRepository.findAll(sort);
	}

	@Override
	public Year save(Year year) {
		return yearRepository.save(year);
	}

	@Override
	public Year findById(Integer id) {
		return yearRepository.findById(id).orElseThrow(() -> new NotFoundException("Year with id: " + id.toString()));
	}

//	@Override
//	public Year updateSelectedYear(Year year) {
//		selectedYear.setYear(year);
//		return getSelectedYear();
//	}

}

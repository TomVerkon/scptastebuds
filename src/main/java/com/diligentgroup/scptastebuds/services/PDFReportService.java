package com.diligentgroup.scptastebuds.services;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;

public interface PDFReportService {

	ByteArrayInputStream generateEventPDFReport(Event event, EventAttendeeProjectionList eaList);
	
	ByteArrayInputStream generateMembersPDFReport(List<Member> members);

	ByteArrayInputStream generateMembershipsPDFReport(List<AnnualMembershipProjection> memberships);

}

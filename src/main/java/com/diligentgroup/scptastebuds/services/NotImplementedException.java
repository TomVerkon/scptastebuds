package com.diligentgroup.scptastebuds.services;

public class NotImplementedException extends RuntimeException {

	private static final long serialVersionUID = -8004117043451232267L;

	public NotImplementedException(String message) {
		super(message);
	}
}

package com.diligentgroup.scptastebuds.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.diligentgroup.scptastebuds.commands.NewEventAttendeeCommand;
import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjection;

public interface EventAttendeeService {

	EventAttendee save(EventAttendee eventAttendee);

	List<EventAttendee> saveAll(List<EventAttendee> eventAttendees);

	EventAttendee findById(Long id);

	List<EventAttendeeProjection> findByEventId(Long eventId, Pageable pageable);

	EventAttendee updateEventAttendeeStatus(Long eventAttendeeId, AttendeeStatus attendeeStatus);

	EventAttendee saveNewEventAttendee(NewEventAttendeeCommand newEventAttendeeCommand);

}

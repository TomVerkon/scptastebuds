package com.diligentgroup.scptastebuds.services;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventStatus;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.EventAttendeeRepository;
import com.diligentgroup.scptastebuds.repositories.EventRepository;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;
import com.diligentgroup.scptastebuds.repositories.projections.EventProjection;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class EventServiceImpl implements EventService {

	private final EventRepository repository;
	private final EventAttendeeRepository eaRepository;
	private final PDFReportService reportService;

	@Transactional(value = TxType.SUPPORTS)
	public List<Event> findByYearAndEventStatus(Year year, EventStatus eventStatus) {
		return StreamSupport.stream(repository.findByYearAndEventStatus(year, eventStatus).spliterator(), false)
				.collect(Collectors.toList());
	}

	@Transactional(value = TxType.SUPPORTS)
	@Override
	public List<EventProjection> findAllProjectionsByYearId(Integer yearId, Pageable pageable) {
		return repository.findAllProjectionsByYearId(yearId, pageable);
	}

	@Transactional
	@Override
	public List<Event> saveAll(List<Event> events) {
		return StreamSupport.stream(repository.saveAll(events).spliterator(), false).collect(Collectors.toList());
	}

	@Transactional
	@Override
	public Event save(Event event) {
		event = repository.save(event);
		if (event.getEventAttendees().isEmpty()) {
			event.getEventAttendees();
		}
		return event;
	}

	/**
	 * @param Long the EventId
	 * @return Event Domain object with eventAttendees collection hydrated
	 */
	@Transactional
	@Override
	public Event findById(Long id) {
		Event event = repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Event with id: " + id.toString()));
		if (event.getEventAttendees().isEmpty()) {
			event.getEventAttendees();
		}
		return event;
	}

	/**
	 * @param Long the EventId
	 * @return Event Domain object without eventAttendees collection
	 */
	@Transactional
	@Override
	public Event findEventOnlyById(Long id) {
		Event event = repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Event with id: " + id.toString()));
		return event;
	}

	@Override
	public void deleteById(Long eventId) {
		repository.deleteById(eventId);
	}

	@Override
	public EventProjection findProjectionById(Long eventId) {
		return repository.findProjectionById(eventId);
	}

	@Override
	@Transactional
	public ByteArrayInputStream generateEventPDFReport(Long eventId) {
		Event event = findById(eventId);
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("lastName").ascending().and(Sort.by("firstName").ascending()));
		EventAttendeeProjectionList eaList = new EventAttendeeProjectionList(
				eaRepository.findByEventId(eventId, pageable));
		return reportService.generateEventPDFReport(event, eaList);
	}

}

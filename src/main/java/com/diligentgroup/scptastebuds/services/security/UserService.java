package com.diligentgroup.scptastebuds.services.security;

import com.diligentgroup.scptastebuds.domain.security.User;

public interface UserService {
	
	public User addUser(User user);
	
	public User findByUsername(String username);

}

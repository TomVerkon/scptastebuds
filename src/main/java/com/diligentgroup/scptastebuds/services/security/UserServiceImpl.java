package com.diligentgroup.scptastebuds.services.security;

import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.security.User;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.repositories.security.UserRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

	private final UserRepository repository;

	@Override
	public User addUser(User user) {
		return repository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		return repository.findByUsername(username)
				.orElseThrow(() -> new NotFoundException("User with name: " + username));
	}

}

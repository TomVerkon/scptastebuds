package com.diligentgroup.scptastebuds.services.security;

import com.diligentgroup.scptastebuds.domain.security.Authority;

public interface AuthorityService {

	public Authority addAuthority(Authority authority);
}

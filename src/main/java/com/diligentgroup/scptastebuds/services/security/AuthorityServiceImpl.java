package com.diligentgroup.scptastebuds.services.security;

import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.security.Authority;
import com.diligentgroup.scptastebuds.repositories.security.AuthorityRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {

	private final AuthorityRepository repository;
	
	@Override
	public Authority addAuthority(Authority authority) {
		return repository.save(authority);
	}
	
}

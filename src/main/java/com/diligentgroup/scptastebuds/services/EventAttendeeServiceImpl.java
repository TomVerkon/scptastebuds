package com.diligentgroup.scptastebuds.services;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.commands.NewEventAttendeeCommand;
import com.diligentgroup.scptastebuds.domain.AttendeeStatus;
import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.repositories.EventAttendeeRepository;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjection;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class EventAttendeeServiceImpl implements EventAttendeeService {

	private final EventAttendeeRepository repository;
	private final AnnualMembershipService annualMembershipService;
	private final EventService eventService;

	@Transactional
	public List<EventAttendee> saveAll(List<EventAttendee> eventAttendees) {
		return StreamSupport.stream(repository.saveAll(eventAttendees).spliterator(), false)
				.collect(Collectors.toList());
	}

	@Transactional
	public List<EventAttendeeProjection> findByEventId(Long eventId, Pageable pageable) {
		return repository.findByEventId(eventId, pageable);
	}

	@Override
	@Transactional
	public EventAttendee save(EventAttendee eventAttendee) {
		eventAttendee
				.setAnnualMembership(annualMembershipService.findById(eventAttendee.getAnnualMembership().getId()));
		eventAttendee.setEvent(eventService.findById(eventAttendee.getEvent().getId()));
		return repository.save(eventAttendee);
	}

	@Transactional(value = TxType.SUPPORTS)
	public EventAttendee findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Event Attendee with id: " + id.toString()));
	}

	@Override
	@Transactional
	public EventAttendee updateEventAttendeeStatus(Long eventAttendeeId, AttendeeStatus attendeeStatus) {
		EventAttendee eventAttendee = findById(eventAttendeeId);
		eventAttendee.setAttendeeStatus(attendeeStatus);
		return save(eventAttendee);
	}

	@Override
	public EventAttendee saveNewEventAttendee(NewEventAttendeeCommand newEventAttendeeCommand) {
		EventAttendee eventAttendee = EventAttendee.builder()
				.annualMembership(annualMembershipService.findById(newEventAttendeeCommand.getAnnualMembershipId()))
				.event(eventService.findById(newEventAttendeeCommand.getEventId()))
				.attendeeStatus(newEventAttendeeCommand.getAttendeeStatus()).build();
		return repository.save(eventAttendee);
	}

}

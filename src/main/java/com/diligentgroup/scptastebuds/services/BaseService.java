//package com.diligentgroup.scptastebuds.services;
//
//import java.util.List;
//
//public interface BaseService<T, ID> {
//
//	List<T> saveAll(List<T> entities);
//
//	List<T> findAll();
//
//	T save(T entity);
//
//	T findById(ID id);
//
//	void deleteById(ID id);
//
//	void deleteAll();
//
//}

package com.diligentgroup.scptastebuds.services;

import static java.awt.Color.BLACK;
import static java.awt.Color.GRAY;
import static java.awt.Color.WHITE;
import static java.util.Objects.isNull;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA_BOLD;
import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA_OBLIQUE;
import static org.vandeseer.easytable.settings.HorizontalAlignment.CENTER;
import static org.vandeseer.easytable.settings.HorizontalAlignment.LEFT;
import static org.vandeseer.easytable.settings.HorizontalAlignment.RIGHT;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.stereotype.Service;
import org.vandeseer.easytable.TableDrawer;
import org.vandeseer.easytable.settings.HorizontalAlignment;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.Table.TableBuilder;
import org.vandeseer.easytable.structure.cell.TextCell;

import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjection;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PDFReportServiceImpl implements PDFReportService {

	private final static Color INFO = new Color(23, 162, 184);
	private final static Color EVEN = new Color(255, 255, 255);
	private final static Color ODD = new Color(225, 243, 250);
	private final static Color SKIN = new Color(40, 167, 69);
	private final static Color HEADER_BACKGROUND = new Color(54, 110, 179);
	private float PADDING = 50f;
	private final PDRectangle LANDSCAPE = new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth());

	@Override
	public ByteArrayInputStream generateMembersPDFReport(List<Member> members) {
		try {
			return new ByteArrayInputStream(
					createAndSavePortraitDocumentWithTables("MembersReport", createMembersTable(members))
							.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Table createMembersTable(List<Member> members) {

		final TableBuilder tableBuilder = Table.builder().addColumnsOfWidth(25, 145, 185, 80, 75).fontSize(10).font(HELVETICA)
				.borderColor(Color.WHITE);

		tableBuilder.addRow(buildColspanFirstHeaderRow("Members ", 5));

		tableBuilder.addRow(Row.builder().add(buildTextCell("")).add(buildHeaderCell("Name", LEFT)).add(buildHeaderCell("Email", LEFT))
				.add(buildHeaderCell("Phone", LEFT)).add(buildHeaderCell("Status", LEFT)).backgroundColor(HEADER_BACKGROUND).build());

		int counter = 1;
		for (Member member : members) {

			tableBuilder.addRow(Row.builder()
					.add(buildTextCell(Integer.toString(counter), LEFT, HELVETICA))
					.add(buildTextCell(member.getLastName() + ", " + member.getFirstName(), LEFT, HELVETICA))
					.add(buildTextCell(member.getEmail(), LEFT, HELVETICA))
					.add(buildTextCell(StringUtils.isBlank(member.getPhone()) ? "N/A" : member.getPhone(), LEFT,
							HELVETICA))
					.add(buildTextCell(member.getMemberStatus().toString(), LEFT, HELVETICA))
					.backgroundColor(counter++ % 2 == 0 ? ODD : EVEN).build());
		}

		return tableBuilder.build();
	}

	@Override
	public ByteArrayInputStream generateMembershipsPDFReport(List<AnnualMembershipProjection> memberships) {
		try {
			return new ByteArrayInputStream(
					createAndSavePortraitDocumentWithTables("MembershipReport", createMembershipsTable(memberships))
							.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Table createMembershipsTable(List<AnnualMembershipProjection> memberships) {

		final TableBuilder tableBuilder = Table.builder().addColumnsOfWidth(30, 150, 200, 80, 50).fontSize(10).font(HELVETICA)
				.borderColor(Color.WHITE);

		tableBuilder.addRow(buildColspanFirstHeaderRow("Memberships ", 5));

		tableBuilder.addRow(Row.builder().add(buildTextCell("")).add(buildHeaderCell("Name", LEFT)).add(buildHeaderCell("Email", LEFT))
				.add(buildHeaderCell("Phone", LEFT)).add(buildHeaderCell("Status", LEFT)).backgroundColor(HEADER_BACKGROUND).build());

		int counter = 1;
		for (AnnualMembershipProjection membership : memberships) {

			String status = (membership.getAnnualMembershipStatus() == null ? " " : membership.getAnnualMembershipStatus().toString());
			tableBuilder.addRow(Row.builder()
					.add(buildTextCell(Integer.toString(counter), LEFT, HELVETICA))
					.add(buildTextCell(membership.getLastName() + ", " + membership.getFirstName(), LEFT, HELVETICA))
					.add(buildTextCell(membership.getEmail(), LEFT, HELVETICA))
					.add(buildTextCell(StringUtils.isBlank(membership.getPhone()) ? "N/A" : membership.getPhone(), LEFT,
							HELVETICA))
					.add(buildTextCell(status, LEFT, HELVETICA))
					.backgroundColor(counter++ % 2 == 0 ? ODD : EVEN).build());
		}

		return tableBuilder.build();
	}

	private Row buildColspanFirstHeaderRow(String text, int colspan) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm");
		return Row.builder()
				.add(buildTextCell(text + "(" + sdf.format(new Date()) + ")", CENTER, HELVETICA_BOLD, colspan, BLACK))
				.backgroundColor(WHITE).build();
	}

	private TextCell buildHeaderCell(String text) {
		return buildHeaderCell(text, CENTER);
	}

	private TextCell buildHeaderCell(String text, HorizontalAlignment alignment) {
		return buildTextCell(text, alignment, HELVETICA_BOLD, Color.WHITE);
	}

	private TextCell buildHeaderCell(String text, HorizontalAlignment alignment, Color textColor) {
		return buildTextCell(text, alignment, HELVETICA_BOLD, textColor);
	}
	
	@Override
	public ByteArrayInputStream generateEventPDFReport(Event event, EventAttendeeProjectionList eaList) {
		try {
			PDDocument document = new PDDocument();
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			Table eventTable = createEventTable(event, eaList);
			Table attendeesTable = createAttendeesTable(eaList);
			createAndSaveDocumentWithTables(document, output, eventTable, attendeesTable);
			ByteArrayInputStream bais = new ByteArrayInputStream(output.toByteArray());
			// Creating Document
			return bais;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private void createAndSaveDocumentWithTables(PDDocument document, ByteArrayOutputStream output, Table... tables)
			throws IOException {

		final PDPage page = new PDPage(PDRectangle.A4);
		document.addPage(page);

		float startY = page.getMediaBox().getHeight() - PADDING;

		try (final PDPageContentStream contentStream = new PDPageContentStream(document, page)) {

			for (final Table table : tables) {

				TableDrawer.builder().page(page).contentStream(contentStream).table(table).startX(PADDING)
						.startY(startY).endY(PADDING).build()
						.draw(() -> document, () -> new PDPage(PDRectangle.A4), PADDING);

				startY -= (table.getHeight() + PADDING);
			}

		}

		document.save(output);
		document.close();

	}

	private Table createAttendeesTable(EventAttendeeProjectionList eaList) {

		final TableBuilder tableBuilder = Table.builder().addColumnsOfWidth(50, 190, 190, 60).fontSize(12)
				.font(HELVETICA).borderColor(Color.WHITE);
		tableBuilder.addRow(Row.builder().add(buildTextCell("Attendees", CENTER, HELVETICA_BOLD, 4, WHITE))
				.backgroundColor(INFO).build());

		int counter = 1;
		for (EventAttendeeProjection projection : eaList.getProjections()) {
			tableBuilder.addRow(Row.builder().add(buildTextCell(String.valueOf(counter++), RIGHT, HELVETICA_OBLIQUE))
					.add(buildTextCell(projection.getLastName() + ", " + projection.getFirstName(), LEFT,
							HELVETICA_OBLIQUE))
					.add(buildTextCell(projection.getEmail(), LEFT, HELVETICA_OBLIQUE))
					.add(buildTextCell(projection.getAttendeeStatus().toString(), CENTER, HELVETICA_OBLIQUE))
					.backgroundColor(ODD).build());

		}

		return tableBuilder.build();
	}

	private Table createEventTable(Event event, EventAttendeeProjectionList eaList) {

		final TableBuilder tableBuilder = Table.builder().addColumnsOfWidth(92, 182, 92, 122).fontSize(12)
				.font(HELVETICA).borderColor(Color.WHITE);

		tableBuilder.addRow(Row.builder().add(buildTextCell("Event Name: ", RIGHT, HELVETICA_BOLD, 1, WHITE))
				.add(buildTextCell(event.getName(), LEFT, HELVETICA_OBLIQUE, 3, WHITE)).backgroundColor(INFO).build());

		tableBuilder.addRow(Row.builder().add(buildTextCell("Location: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(event.getLocation(), HELVETICA_OBLIQUE))
				.add(buildTextCell("Event Status: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(event.getEventStatus().toString(), HELVETICA_OBLIQUE)).backgroundColor(ODD).build());

		DecimalFormat df = new DecimalFormat("$#,##0.00");
		tableBuilder
				.addRow(Row.builder().add(buildTextCell("Date & Time: ", RIGHT, HELVETICA_BOLD, GRAY))
						.add(buildTextCell(event.getEventDate() + " " + event.getEventStartTime() + " - "
								+ event.getEventEndTime(), HELVETICA_OBLIQUE))
						.add(buildTextCell("Tkt Cost: ", RIGHT, HELVETICA_BOLD, GRAY))
						.add(buildTextCell(df.format(event.getTicketCost()), HELVETICA_OBLIQUE)).backgroundColor(ODD)
						.build());

		tableBuilder.addRow(Row.builder().add(buildTextCell("Max Tkts: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(event.getMaxAttendees().toString(), HELVETICA_OBLIQUE))
				.add(buildTextCell("# Signedup: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(eaList.getSignedUpCount().toString(), HELVETICA_OBLIQUE)).backgroundColor(ODD)
				.build());

		tableBuilder.addRow(Row.builder().add(buildTextCell("# Paid: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(eaList.getPaidCount().toString(), HELVETICA_OBLIQUE))
				.add(buildTextCell("# UNPAID: ", RIGHT, HELVETICA_BOLD, GRAY))
				.add(buildTextCell(eaList.getUnpaidCount().toString(), HELVETICA_OBLIQUE)).backgroundColor(ODD)
				.build());

//		tableBuilder.addRow(Row.builder().add(buildLATextCell(event.getLocation(), 2)).add(buildLATextCell())
//				.add(buildLATextCell(String.valueOf(event.getMaxAttendees()))).backgroundColor(EVEN).build());

		return tableBuilder.build();
	}

	/**
	 * @param text the cell text content
	 * @return the constructed TextCell LEFT aligned, HELVETICA font, 1 column,
	 *         BLACK text
	 * @see HorizontalAlignment
	 * @see PDType1Font
	 * @see Color
	 */
	private TextCell buildTextCell(String text) {
		return buildTextCell(text, null, null, null, null);
	}

	/**
	 * @param text the cell text content
	 * @param font the font to use for the text, default HELVETICA
	 * @return the constructed TextCell LEFT aligned, 1 column, BLACK text
	 * @see HorizontalAlignment
	 * @see PDType1Font
	 * @see Color
	 */
	private TextCell buildTextCell(String text, PDType1Font font) {
		return buildTextCell(text, null, font, null, null);
	}

	/**
	 * @param text    the cell text content
	 * @param font    the font to use for the text, default HELVETICA
	 * @param colSpan the number of columns to span for this content, default 1
	 * @return the constructed TextCell LEFT aligned, colSpan columns, BLACK text
	 * @see HorizontalAlignment
	 * @see PDType1Font
	 * @see Color
	 */
	private TextCell buildTextCell(String text, PDType1Font font, Integer colSpan) {
		return buildTextCell(text, null, font, colSpan, null);
	}

	private TextCell buildTextCell(String text, HorizontalAlignment alignment, PDType1Font font) {
		return buildTextCell(text, alignment, font, 1, BLACK);
	}

	private TextCell buildTextCell(String text, HorizontalAlignment alignment, PDType1Font font, Color textColor) {
		return buildTextCell(text, alignment, font, 1, textColor);
	}

	/**
	 * @param text      the cell text content
	 * @param alignment the text alignment in the cell, default LEFT
	 * @param font      the font to use for the text, default HELVETICA
	 * @param colSpan   the number of columns to span for this content, default 1
	 * @param textColor the color to use for text, default BLACK
	 * @return the constructed TextCell
	 * @see HorizontalAlignment
	 * @see PDType1Font
	 * @see Color
	 */
	private TextCell buildTextCell(String text, HorizontalAlignment alignment, PDType1Font font, Integer colSpan,
			Color textColor) {
		return TextCell.builder().text(text).colSpan(isNull(colSpan) ? 1 : colSpan.intValue())
				.horizontalAlignment((isNull(alignment)) ? LEFT : alignment).borderWidth(1)
				.font(isNull(font) ? HELVETICA : font).textColor(isNull(textColor) ? BLACK : textColor).build();
	}

	private ByteArrayOutputStream createAndSaveLandscapeDocumentWithTables(String reportTitle, Table... tables)
			throws IOException {
		return createAndSaveDocumentWithTables(reportTitle, new PDPage(LANDSCAPE), tables);
	}

	private ByteArrayOutputStream createAndSavePortraitDocumentWithTables(String reportTitle, Table... tables)
			throws IOException {
		return createAndSaveDocumentWithTables(reportTitle, new PDPage(PDRectangle.A4), tables);
	}

	/**
	 * @param page
	 * @param tables
	 * @return
	 * @throws IOException
	 */
	private ByteArrayOutputStream createAndSaveDocumentWithTables(String reportTitle, PDPage page, Table... tables)
			throws IOException {

		// get MediaBox dims from passed page for subsequent pages
		float pageHeight = page.getMediaBox().getHeight();
		float pageWidth = page.getMediaBox().getWidth();

		PDDocument document = new PDDocument();
		document.getDocumentInformation().setTitle(reportTitle);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		document.addPage(page);

		float startY = page.getMediaBox().getHeight() - PADDING;

		try (final PDPageContentStream contentStream = new PDPageContentStream(document, page)) {

			for (final Table table : tables) {

				TableDrawer.builder().page(page).contentStream(contentStream).table(table).startX(PADDING)
						.startY(startY).endY(PADDING).build()
						.draw(() -> document, () -> new PDPage(new PDRectangle(pageWidth, pageHeight)), PADDING);

				startY -= (table.getHeight() + PADDING);
			}

		}

		document.save(output);
		document.close();
		return output;

	}

}

package com.diligentgroup.scptastebuds.config;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.diligentgroup.scptastebuds.domain.security.Authority;
import com.diligentgroup.scptastebuds.domain.security.User;
import com.diligentgroup.scptastebuds.repositories.security.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TBUserDetailsService implements UserDetailsService {

	private final UserRepository repository;

	@Transactional()
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = repository.findByUsername(username).orElseThrow(() -> {
			return new UsernameNotFoundException("User name: " + username + " not found");
		});

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(), user.isAccountNonLocked(),
				convertToSpringAuthorities(user.getAuthorities()));
	}

	private Collection<? extends GrantedAuthority> convertToSpringAuthorities(Set<Authority> authorities) {
		if (authorities != null && !authorities.isEmpty()) {
			return authorities.stream().map(Authority::getPermission).map(SimpleGrantedAuthority::new)
					.collect(Collectors.toSet());
		} else {
			return new HashSet<>();
		}
	}

}

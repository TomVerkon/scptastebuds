package com.diligentgroup.scptastebuds.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests(authorize -> {
			authorize.antMatchers("/", "/webjars/**", "/css/**").permitAll();
			authorize.antMatchers("/favicon.ico", "/js/**", "/images/**").permitAll();
//			authorize.mvcMatchers(HttpMethod.GET, "/member/list").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/member/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/member/save").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/member/{id}/edit").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.DELETE, "/member/{id}/delete").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/member/{id}/view").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/member/report").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/membership/list").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/membership/{id}/view").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/membership/{id}/edit").hasAnyRole("ADMIN","CFO");
//			authorize.mvcMatchers(HttpMethod.GET, "/membership/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/membership/save").hasAnyRole("ADMIN","CFO");
//			authorize.mvcMatchers(HttpMethod.GET, "/membership/report").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/list").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/{id}/view").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/event/save").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/{id}/edit").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/{id}/attendee/list").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/attendee/{attendeeId}/view").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/attendee/{attendeeId}/edit").hasAnyRole("ADMIN","CFO");
//			authorize.mvcMatchers(HttpMethod.POST, "/event/attendee/save").hasAnyRole("ADMIN","CFO");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/{eventId}/attendee/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/event/attendee/new").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.DELETE, "/event/{eventId}/delete").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/event/{eventId}/report").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/year/list").hasAnyRole("ADMIN","CFO","BDMBR");
//			authorize.mvcMatchers(HttpMethod.GET, "/year/edit").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/year/updateActiveYear").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.GET, "/year/add").hasRole("ADMIN");
//			authorize.mvcMatchers(HttpMethod.POST, "/year/save").hasRole("ADMIN");
			//authorize.antMatchers("/**").permitAll();
		}).authorizeRequests().anyRequest().authenticated().and().formLogin().and().httpBasic();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
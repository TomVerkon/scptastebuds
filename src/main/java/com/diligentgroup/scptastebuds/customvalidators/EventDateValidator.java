package com.diligentgroup.scptastebuds.customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;

public class EventDateValidator implements ConstraintValidator<EventDateConstraint, String> {

	@Override
	public void initialize(EventDateConstraint eventDate) {
	}

	@Override
	public boolean isValid(String eventDateField, ConstraintValidatorContext cxt) {
		return eventDateField != null && eventDateField.substring(0, 4).equals(ActiveYearThreadLocal.get().toString());
	}
}

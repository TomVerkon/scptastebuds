package com.diligentgroup.scptastebuds.customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class EventDatesMatchValidator implements ConstraintValidator<EventDatesMatchConstraint, Object> {

	private String field;
	private String fieldMatch;

	public void initialize(EventDatesMatchConstraint constraintAnnotation) {
		this.field = constraintAnnotation.field();
		this.fieldMatch = constraintAnnotation.fieldMatch();
	}

	public boolean isValid(Object value, ConstraintValidatorContext context) {

		Object fieldValue = new BeanWrapperImpl(value).getPropertyValue(field);
		Object fieldMatchValue = new BeanWrapperImpl(value).getPropertyValue(fieldMatch);

		if (fieldValue != null && fieldValue instanceof String && fieldMatchValue != null
				&& fieldMatchValue instanceof Integer) {
			String strFieldValue = (String) fieldValue;
			return strFieldValue.substring(0, 4).equals(fieldMatchValue.toString());
		} else {
			return false;
		}
	}
}

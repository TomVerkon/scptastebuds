package com.diligentgroup.scptastebuds.web.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.diligentgroup.scptastebuds.commands.AnnualMembershipCommand;
import com.diligentgroup.scptastebuds.commands.EventAttendeeCommand;
import com.diligentgroup.scptastebuds.commands.EventCommand;
import com.diligentgroup.scptastebuds.commands.MemberCommand;
import com.diligentgroup.scptastebuds.commands.YearCommand;
import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.EmailTarget;
import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjection;

@Mapper(componentModel = "spring")
public interface DomainCommandMapper {

	MemberCommand memberToMemberCommand(Member member);

	Member memberCommandToMember(MemberCommand memberCommand);

	YearCommand yearToYearCommand(Year year);

	Year yearCommandToYear(YearCommand yearCommand);

	List<YearCommand> yearsToYearCommands(List<Year> years);
	
	List<EmailTarget> annualMembershipProjectionsToEmailTarget(List<AnnualMembershipProjection> projections);

	List<EmailTarget> eventAttendeeProjectionsToEmailTarget(List<EventAttendeeProjection> projections);

	@Mapping(source = "member", target = "memberCommand")
	@Mapping(source = "year", target = "yearCommand")
	AnnualMembershipCommand annualMembershipToAnnualMembershipCommand(AnnualMembership annualMembership);

	@Mapping(source = "memberCommand", target = "member")
	@Mapping(source = "yearCommand", target = "year")
	AnnualMembership annualMembershipCommandToAnnualMembership(AnnualMembershipCommand annualMembershipCommand);

	@Mapping(source = "year", target = "yearCommand")
	EventCommand eventToEventCommand(Event event);

	List<EventAttendeeCommand> eventAttendeesToEventAttendeeCommands(List<EventAttendee> eventAttendees);

	@Mapping(source = "yearCommand", target = "year")
	Event eventCommandToEvent(EventCommand eventCommand);

	@Mapping(source = "annualMembershipCommand", target = "annualMembership")
	@Mapping(source = "eventCommand", target = "event")
	EventAttendee eventAttendeeCommandToEventAttendee(EventAttendeeCommand eventAttendeeCommand);

	@Mapping(source = "annualMembership", target = "annualMembershipCommand")
	@Mapping(source = "event", target = "eventCommand")
	EventAttendeeCommand eventAttendeeToEventAttendeeCommand(EventAttendee eventAttendee);

}

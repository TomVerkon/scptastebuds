package com.diligentgroup.scptastebuds.web.controllers;

import java.io.ByteArrayInputStream;

import javax.validation.Valid;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.diligentgroup.scptastebuds.commands.EmailAddressesCommand;
import com.diligentgroup.scptastebuds.commands.EventAttendeeCommand;
import com.diligentgroup.scptastebuds.commands.EventCommand;
import com.diligentgroup.scptastebuds.commands.NewEventAttendeeCommand;
import com.diligentgroup.scptastebuds.commands.YearCommand;
import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.domain.Event;
import com.diligentgroup.scptastebuds.domain.EventAttendee;
import com.diligentgroup.scptastebuds.repositories.projections.EventAttendeeProjectionList;
import com.diligentgroup.scptastebuds.services.AnnualMembershipService;
import com.diligentgroup.scptastebuds.services.EventAttendeeService;
import com.diligentgroup.scptastebuds.services.EventService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/event")
@Controller
public class EventController extends ControllerActions {

	public final static String ACTIVE_NAV = "event";

	private final EventService eventService;
	private final DomainCommandMapper mapper;
	private final EventAttendeeService eventAttendeeService;
	private final AnnualMembershipService annualMembershipService;

	// get an event list
	@PreAuthorize("hasAuthority('event.read')")
	@GetMapping("/list")
	public String getEventList(Model model) {
		Pageable pageable = PageRequest.of(0, 500, Sort.by("eventDate").ascending());
		model.addAttribute("eventProjections", eventService.findAllProjectionsByYearId(getSelectedYearId(), pageable));
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-list";
	}

	// view an event and attendees
	@PreAuthorize("hasAuthority('event.read')")
	@GetMapping("/{eventId}/view")
	public String viewEvent(@PathVariable Long eventId, Model model) {
		EventCommand eventCommand = mapEventCommand(eventService.findById(eventId));
		model.addAttribute("eventCommand", eventCommand);
		model.addAttribute("action", VIEW);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-form";
	}

	// add a new event
	@PreAuthorize("hasAuthority('event.create')")
	@GetMapping("/add")
	public String addEvent(@RequestAttribute("activeYear") Integer activeYear, Model model) {
		EventCommand eventCommand = EventCommand.builder()
				.yearCommand(YearCommand.builder().id(ActiveYearThreadLocal.get()).build()).build();
		model.addAttribute("eventCommand", eventCommand);
		model.addAttribute("action", ADD);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-form";
	}

	// save a new or updated event
	@PreAuthorize("hasAnyAuthority('event.create', 'event.update')")
	@PostMapping("/save")
	public String saveUpdateEvent(@Valid EventCommand eventCommand, Model model, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "event/event-form";
		}
		Event event = mapEvent(eventCommand);
		event.setYear(yearService.findById(getSelectedYearId()));
		eventCommand = mapEventCommand(eventService.save(event));
		return "redirect:/event/" + eventCommand.getId() + "/view";
	}

	// edit an existing event
	@PreAuthorize("hasAuthority('event.update')")
	@GetMapping("/{eventId}/edit")
	public String editEvent(@PathVariable Long eventId, Model model) {
		model.addAttribute("eventCommand", mapEventCommand(eventService.findById(eventId)));
		model.addAttribute("action", EDIT);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-form";
	}

	// get events attendee list
	@PreAuthorize("hasAuthority('event.attendee.read')")
	@GetMapping("/{eventId}/attendee/list")
	public String getEventAttendeeList(@PathVariable Long eventId, Model model) {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("lastName").ascending().and(Sort.by("firstName").ascending()));
		model.addAttribute("eventProjection", eventService.findProjectionById(eventId));
		model.addAttribute("eventAttendeeProjectionList",
				new EventAttendeeProjectionList(eventAttendeeService.findByEventId(eventId, pageable)));
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-attendee-list";
	}

//	@GetMapping("/attendee/{attendeeId}/view")
//	public String viewEventAttendee(@PathVariable Long attendeeId, Model model) {
//		EventAttendeeCommand eventAttendeeCommand = eventAttendeeService.findById(attendeeId);
//		model.addAttribute("eventAttendeeCommand", eventAttendeeCommand);
//		model.addAttribute("action", VIEW);
//		model.addAttribute("active", ACTIVE_NAV);
//		return "/event/event-attendee-form";
//	}

	@PreAuthorize("hasAuthority('event.attendee.read')")
	@GetMapping("/attendee/{attendeeId}/view")
	public String viewEventAttendee(@PathVariable Long attendeeId, Model model) {
		EventAttendeeCommand eventAttendeeCommand = mapEventAttendeeCommand(eventAttendeeService.findById(attendeeId));
		model.addAttribute("eventAttendeeCommand", eventAttendeeCommand);
		model.addAttribute("action", VIEW);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-attendee-form";
	}

	@PreAuthorize("hasAuthority('event.attendee.update')")
	@GetMapping("/attendee/{attendeeId}/edit")
	public String editEventAttendee(@PathVariable Long attendeeId, Model model) {
		EventAttendeeCommand eventAttendeeCommand = mapEventAttendeeCommand(eventAttendeeService.findById(attendeeId));
		model.addAttribute("eventAttendeeCommand", eventAttendeeCommand);
		model.addAttribute("action", EDIT);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-attendee-form";
	}

	// save an updated event attendee
	@PreAuthorize("hasAnyAuthority('event.attendee.update', 'event.attendee.create')")
	@PostMapping("/attendee/save")
	public String saveUpdateEventAttendeeStatus(@Valid EventAttendeeCommand eventAttendeeCommand,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "event/event-attendee-form";
		}
		eventAttendeeCommand = mapEventAttendeeCommand(
				eventAttendeeService.save(mapEventAttendee(eventAttendeeCommand)));
		return "redirect:/event/" + eventAttendeeCommand.getEventCommand().getId() + "/attendee/list";
	}

	// get events attendee list
	@PreAuthorize("hasAuthority('event.attendee.create')")
	@GetMapping("/{eventId}/attendee/add")
	public String addEventAttendee(@PathVariable Long eventId, Model model) {
		NewEventAttendeeCommand newEventAttendeeCommand = new NewEventAttendeeCommand();
		EventCommand eventCommand = mapEventCommand(eventService.findById(eventId));
		newEventAttendeeCommand.setEventId(eventCommand.getId());
		newEventAttendeeCommand.setEventName(eventCommand.getName());
		newEventAttendeeCommand.setEligibleMembers(
				annualMembershipService.getEligibleMembershipsByYearAndEvent(getSelectedYearId(), eventId));
		model.addAttribute("newEventAttendeeCommand", newEventAttendeeCommand);
		model.addAttribute("action", ADD);
		model.addAttribute("active", ACTIVE_NAV);
		return "event/event-attendee-add-form";
	}

	// save a new event attendee
	@PostMapping("/attendee/new")
	public String saveNewEventAttendee(@Valid NewEventAttendeeCommand newEventAttendeeCommand,
			BindingResult bindingResult) {
		eventAttendeeService.saveNewEventAttendee(newEventAttendeeCommand);
		return "redirect:/event/" + newEventAttendeeCommand.getEventId() + "/attendee/list";
	}

	@PreAuthorize("hasAuthority('event.attendee.delete')")
	@DeleteMapping("/{eventId}/delete")
	public String deleteEvent(@PathVariable Long eventId, Model model) {
		eventService.deleteById(eventId);
		return "redirect:/event/list";
	}

	@PreAuthorize("hasAuthority('event.read')")
	@GetMapping(value = "/{eventId}/pdfreport", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> eventPDFReport(@PathVariable Long eventId) {

		ByteArrayInputStream bis = eventService.generateEventPDFReport(eventId);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=eventReport.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	private EventCommand mapEventCommand(Event event) {
		EventCommand eventCommand = mapper.eventToEventCommand(event);
		eventCommand.setEventAttendeeCommands(mapper.eventAttendeesToEventAttendeeCommands(event.getEventAttendees()));
		return eventCommand;
	}

	private Event mapEvent(EventCommand eventCommand) {
		return mapper.eventCommandToEvent(eventCommand);
	}

	private EventAttendee mapEventAttendee(EventAttendeeCommand eventAttendeeCommand) {
		return mapper.eventAttendeeCommandToEventAttendee(eventAttendeeCommand);
	}

	private EventAttendeeCommand mapEventAttendeeCommand(EventAttendee eventAttendee) {
		return mapper.eventAttendeeToEventAttendeeCommand(eventAttendee);
	}
	
	@PreAuthorize("hasAuthority('event.attendee.email')")
	@GetMapping("/{eventId}/attendee/mailtos")
	public String feedMembers(@PathVariable Long eventId, Model model) {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("lastName").ascending().and(Sort.by("firstName").ascending()));
		model.addAttribute("eventProjection", eventService.findProjectionById(eventId));
		model.addAttribute("emailTargets", mapper.eventAttendeeProjectionsToEmailTarget(
				eventAttendeeService.findByEventId(eventId, pageable)));
		model.addAttribute("emailAddysCommand", new EmailAddressesCommand());
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("returnLink", "/event/${eventId}/attendee/list");
		return "event/event-attendee-email-list";
	}

	

//	@GetMapping("/{id}/eligibleAttendees")
//	@ResponseBody
//	public List<AnnualMembershipProjection> getEligibleAttendees(@PathVariable Long id) {
//		return annualMembershipService.getEligibleMembershipsByYearAndEvent(Integer yearId, Long eventId);
//	}

}

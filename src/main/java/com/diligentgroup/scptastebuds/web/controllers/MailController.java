package com.diligentgroup.scptastebuds.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.diligentgroup.scptastebuds.commands.EmailAddressesCommand;
import com.diligentgroup.scptastebuds.services.AnnualMembershipService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/mail")
@Controller
public class MailController extends ControllerActions {
	
	private final static String ACTIVE_NAV = "mail";

	private final AnnualMembershipService service;
	private final DomainCommandMapper mapper;

	@PostMapping("/mailtos")
	String buildMailToString(EmailAddressesCommand mailTos, Model model) {
		model.addAttribute("mailTos", mailTos);
		model.addAttribute("active", ACTIVE_NAV);
		return "mail/tos";
	}
	
	

}

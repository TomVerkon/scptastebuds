package com.diligentgroup.scptastebuds.web.controllers;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.diligentgroup.scptastebuds.commands.AnnualMembershipCommand;
import com.diligentgroup.scptastebuds.commands.EmailAddressesCommand;
import com.diligentgroup.scptastebuds.commands.MemberCommand;
import com.diligentgroup.scptastebuds.commands.YearCommand;
import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.domain.AnnualMembership;
import com.diligentgroup.scptastebuds.domain.AnnualMembershipStatus;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjection;
import com.diligentgroup.scptastebuds.repositories.projections.AnnualMembershipProjectionList;
import com.diligentgroup.scptastebuds.services.AnnualMembershipService;
import com.diligentgroup.scptastebuds.services.MemberService;
import com.diligentgroup.scptastebuds.services.PDFReportService;
import com.diligentgroup.scptastebuds.services.YearService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/membership")
@Controller
public class AnnualMembershipController extends ControllerActions {

	protected final static String ACTIVE_NAV = "membership";
	private final DomainCommandMapper mapper;
	private final AnnualMembershipService service;
	private final MemberService mbrService;
	private final PDFReportService pdfService;
	private final YearService yrService;

	@PreAuthorize("hasAuthority('membership.read')")
	@GetMapping("/list")
	public String getAnnualMembershipList(Model model) {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("m.lastName").ascending().and(Sort.by("m.firstName").ascending()));
		model.addAttribute("annualMembershipProjectionList", new AnnualMembershipProjectionList(
				service.getMembershipProjectionsByYear(getSelectedYearId(), pageable)));
		model.addAttribute("active", ACTIVE_NAV);
		return "membership/membership-list";
	}

	@PreAuthorize("hasAuthority('membership.read')")
	@GetMapping("/{id}/view")
	public String viewAnnualMember(@PathVariable Long id, Model model) {
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("action", VIEW);
		model.addAttribute("membershipCommand", getAnnualMembershipCommandById(id));
		return "membership/membership-form";
	}

	private AnnualMembershipCommand getAnnualMembershipCommandById(Long id) {
		return mapper.annualMembershipToAnnualMembershipCommand(service.findById(id));
	}

	@PreAuthorize("hasAuthority('membership.update')")
	@GetMapping("/{id}/edit")
	public String editAnnualMember(@PathVariable Long id, Model model) {
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("action", EDIT);
		model.addAttribute("membershipCommand", getAnnualMembershipCommandById(id));
		return "membership/membership-form";
	}

	@PreAuthorize("hasAuthority('membership.create')")
	@GetMapping("/{id}/add")
	public String addAnnualMember(@PathVariable Long id, Model model) {
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("action", ADD);
		MemberCommand memberCommand = getMemberCommandById(id);
		AnnualMembershipCommand membershipCommand = AnnualMembershipCommand.builder()
				.annualMembershipStatus(AnnualMembershipStatus.UNPAID).memberCommand(memberCommand)
				.yearCommand(YearCommand.builder().id(ActiveYearThreadLocal.get()).build()).build();
		model.addAttribute("membershipCommand", membershipCommand);
		return "membership/membership-form";
	}

	private MemberCommand getMemberCommandById(Long id) {
		return mapper.memberToMemberCommand(mbrService.findById(id));
	}

	@PreAuthorize("hasAnyAuthority('membership.update', 'membership.create')")
	@PostMapping("/save")
	public String updateMembershipStatus(@Valid AnnualMembershipCommand annualMembershipCommand,
			BindingResult bindingResult) {
		log.debug("updateMembershipStatus");
		if (bindingResult.hasErrors()) {
			return "membership/membership-form";
		}
		log.debug("amMapperService.updateMembershipStatus");
		if (!annualMembershipCommand.isNew()) {
			service.updateMembershipStatus(annualMembershipCommand.getId(),
					annualMembershipCommand.getAnnualMembershipStatus());
		} else {
			annualMembershipCommand.setYearCommand(
					mapper.yearToYearCommand(yrService.findById(annualMembershipCommand.getYearCommand().getId())));
			annualMembershipCommand
					.setMemberCommand(getMemberCommandById(annualMembershipCommand.getMemberCommand().getId()));
			annualMembershipCommand = mapper.annualMembershipToAnnualMembershipCommand(
					service.save(annualMembershipCommandToAnnualMembership(annualMembershipCommand)));
		}
		return "redirect:/membership/" + annualMembershipCommand.getId() + "/view";
	}

	private AnnualMembership annualMembershipCommandToAnnualMembership(
			AnnualMembershipCommand annualMembershipCommand) {
		return mapper.annualMembershipCommandToAnnualMembership(annualMembershipCommand);
	}

	@PreAuthorize("hasAuthority('membership.report')")
	@GetMapping(value = { "/report", "/report/" }, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getMembershipsReport() {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("m.lastName").ascending().and(Sort.by("m.firstName").ascending()));
		List<AnnualMembershipProjection> memberships = service.getMembershipProjectionsByYear(getSelectedYearId(),
				pageable);
		ByteArrayInputStream bis = pdfService.generateMembershipsPDFReport(memberships);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmm");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=members-" + sdf.format(new Date()) + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

	@PreAuthorize("hasAuthority('membership.email')")
	@GetMapping("/mailtos")
	public String feedMembers(Model model) {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("m.lastName").ascending().and(Sort.by("m.firstName").ascending()));
		model.addAttribute("emailTargets", mapper.annualMembershipProjectionsToEmailTarget(
				service.getMembershipProjectionsByYear(getSelectedYearId(), pageable)));
		model.addAttribute("emailAddysCommand", new EmailAddressesCommand());
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("returnLink", "/membership/list");
		return "membership/membership-email-list";
	}

}

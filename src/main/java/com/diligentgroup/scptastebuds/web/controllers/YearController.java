package com.diligentgroup.scptastebuds.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.diligentgroup.scptastebuds.commands.ChangeActiveYearCommand;
import com.diligentgroup.scptastebuds.commands.YearCommand;
import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.domain.SelectedYearId;
import com.diligentgroup.scptastebuds.domain.Year;
import com.diligentgroup.scptastebuds.services.YearService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/year")
@Controller
public class YearController extends ControllerActions {

	protected final static String ACTIVE_NAV = "year";

	private final YearService yearService;
	private final DomainCommandMapper mapper;
	private final SelectedYearId selectedYear;

	// get an event list
	@GetMapping("/list")
	public String getYearList(Model model) {
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("yearCommands", mapper.yearsToYearCommands(yearService.findAll(Sort.by("id").ascending())));
		model.addAttribute("currentlySelectedYear", ActiveYearThreadLocal.get());
		return "year/year-list";
	}

	@GetMapping("/edit")
	public String changeActiveYear(Model model) {
		model.addAttribute("active", ACTIVE_NAV);
		model.addAttribute("action", ControllerActions.CHANGE_ACTIVE_YEAR);
		model.addAttribute("yearCommands", mapper.yearsToYearCommands(yearService.findAll(Sort.by("id").ascending())));
		model.addAttribute("command", new ChangeActiveYearCommand(ActiveYearThreadLocal.get()));
		return "year/year-edit";
	}

	@PostMapping("/updateActiveYear")
	public String updateActiveYear(ChangeActiveYearCommand changeActiveYearCommand) {
		selectedYear.setYearId(changeActiveYearCommand.getSelectedYear());
		ActiveYearThreadLocal.set(selectedYear.getYearId());
		return "redirect:/year/list";
	}

	@GetMapping("/add")
	public String addYear(Sort sort) {
		List<Year> years = yearService.findAll(Sort.by("id").ascending());
		Year latestYear = years.get(years.size() - 1);
		Year newestYear = Year.builder().id(latestYear.getId() + 1).build();
		yearService.save(newestYear);
		return "redirect:/year/list";
	}

	@PostMapping("/save")
	public String saveYear(@Valid YearCommand yearCommand, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "year/year-form";
		}
		yearService.save(mapper.yearCommandToYear(yearCommand));
		return "redirect:/year/list";
	}

}

package com.diligentgroup.scptastebuds.web.controllers;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.diligentgroup.scptastebuds.commands.MemberCommand;
import com.diligentgroup.scptastebuds.domain.Member;
import com.diligentgroup.scptastebuds.repositories.NotFoundException;
import com.diligentgroup.scptastebuds.services.MemberService;
import com.diligentgroup.scptastebuds.services.PDFReportService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/member")
@Controller
public class MemberController extends ControllerActions {

	private final static String ACTIVE_NAV = "member";

	private final MemberService memberService;
	private final DomainCommandMapper mapper;
	private final PDFReportService pdfService;

	private boolean duplicateInfo;

	@PreAuthorize("hasAuthority('member.read')")
	@GetMapping("/list")
	public String getMemberList(Model model) {
		Pageable pageable = PageRequest.of(0, 500,
				Sort.by("lastName").ascending().and(Sort.by("firstName").ascending()));
		List<MemberCommand> memberCommands = StreamSupport.stream(memberService.findAll(pageable).spliterator(), false)
				.map(mapper::memberToMemberCommand).collect(Collectors.toList());
		model.addAttribute("memberCommands", memberCommands);
		model.addAttribute("active", ACTIVE_NAV);
		return "member/member-list";
	}

	@PreAuthorize("hasAuthority('member.create')")
	@GetMapping("/add")
	public String addMember(Model model) {
		model.addAttribute("memberCommand", new MemberCommand());
		model.addAttribute("action", ADD);
		model.addAttribute("active", ACTIVE_NAV);
		return "member/member-form";
	}

	@PreAuthorize("hasAnyAuthority('member.create', 'member.update')")
	@PostMapping("/save")
	public String saveUpdateMember(@Valid MemberCommand memberCommand, BindingResult bindingResult) {
		boolean duplicate = true;
		if (memberCommand.getId() == null) {
			try {
				memberService.findByEmailAndFirstName(memberCommand.getEmail(), memberCommand.getFirstName());
			} catch (NotFoundException e) {
				duplicate = false;
			} finally {
				if (duplicate) {
					ObjectError error = new ObjectError("global",
							"A member with this email and first name already exists");
					bindingResult.addError(error);
				}
			}
		} else {
			Member questionableMember = null;
			try {
				questionableMember = memberService.findByEmailAndFirstName(memberCommand.getEmail(), memberCommand.getFirstName());
			} catch (NotFoundException e) {
				duplicate = false;
			} finally {
				if (duplicate && questionableMember.getId() != memberCommand.getId()) {
					ObjectError error = new ObjectError("global",
							"A member with this email and first name already exists");
					bindingResult.addError(error);
				}
			}			
		}
		if (bindingResult.hasErrors()) {
			return "member/member-form";
		}
		memberCommand = toCommand(memberService.save(toMember(memberCommand)));
		return "redirect:/member/" + memberCommand.getId() + "/view";
	}

	private Member toMember(MemberCommand memberCommand) {
		return mapper.memberCommandToMember(memberCommand);
	}

	private MemberCommand toCommand(Member member) {
		return mapper.memberToMemberCommand(member);
	}

	@PreAuthorize("hasAuthority('member.update')")
	@GetMapping("/{id}/edit")
	public String editMember(@PathVariable Long id, Model model) {
		model.addAttribute("memberCommand", toCommand(memberService.findById(id)));
		model.addAttribute("action", EDIT);
		model.addAttribute("active", ACTIVE_NAV);
		return "member/member-form";
	}

	@PreAuthorize("hasAuthority('member.delete')")
	@DeleteMapping("/{id}/delete")
	public String deleteMember(@PathVariable Long id) {
		memberService.deleteById(id);
		return "redirect:/member/list";
	}

	@PreAuthorize("hasAuthority('member.read')")
	@GetMapping("/{id}/view")
	public String viewMember(@PathVariable Long id, Model model) {
		model.addAttribute("memberCommand", toCommand(memberService.findById(id)));
		model.addAttribute("action", VIEW);
		model.addAttribute("active", ACTIVE_NAV);
		return "member/member-form";
	}

	@PreAuthorize("hasAuthority('member.report')")
	@GetMapping(value = { "/report", "/report/" }, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getMemberStandingsReport() {
		Pageable pageable = PageRequest.of(0, 5000,
				Sort.by("lastName").ascending().and(Sort.by("firstName").ascending()));

		List<Member> members = memberService.findAll(pageable);

		ByteArrayInputStream bis = pdfService.generateMembersPDFReport(members);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmm");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=members-" + sdf.format(new Date()) + ".pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}

}

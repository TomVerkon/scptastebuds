package com.diligentgroup.scptastebuds.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.diligentgroup.scptastebuds.domain.SelectedYearId;
import com.diligentgroup.scptastebuds.services.YearService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

public abstract class ControllerActions {

	@Autowired
	SelectedYearId selectedYear;
	@Autowired
	YearService yearService;
	@Autowired
	DomainCommandMapper mapper;

	public static final String ADD = "Add";
	public static final String EDIT = "Edit";
	public static final String VIEW = "View";
	public static final String CHANGE_ACTIVE_YEAR = "Change Active Year";

	Integer getSelectedYearId() {
		return selectedYear.getYearId();
	}
	
	@ExceptionHandler(org.springframework.security.access.AccessDeniedException.class)
	public ModelAndView handleIOException(org.springframework.security.access.AccessDeniedException ex) {
	    ModelAndView model = new ModelAndView("error");
	    model.addObject("header", "An Error has occurred" );
	    model.addObject("title", ex.getMessage());
	    model.addObject("exception", "You don't have the appropriate credentials to access this page");
	 
	    return model;
	}

	@ExceptionHandler(java.lang.Exception.class)
	public ModelAndView handleIOException(java.lang.Exception ex) {
	    ModelAndView model = new ModelAndView("error");
	    model.addObject("header", "An Error has occurred" );
	    model.addObject("title", ex.getMessage());
	    model.addObject("exception", "unknown");
	 
	    return model;
	}

}

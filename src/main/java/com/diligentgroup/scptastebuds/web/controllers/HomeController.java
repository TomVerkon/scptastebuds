package com.diligentgroup.scptastebuds.web.controllers;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController extends ControllerActions {

	@GetMapping({ "", "/" })
	String goHome(HttpSession session) {
//		YearCommand yearCommand = getSelectedYear();
//		System.out.println(yearCommand);
		Enumeration<String> attributeNames = session.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String attributeName = attributeNames.nextElement();
			Object object = session.getAttribute(attributeName);
			System.out.println(attributeName + ": " + object.toString());
		}
		return "index";
	}

}

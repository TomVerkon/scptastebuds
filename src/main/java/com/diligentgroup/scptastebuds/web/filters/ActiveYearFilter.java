package com.diligentgroup.scptastebuds.web.filters;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.diligentgroup.scptastebuds.domain.ActiveYearThreadLocal;
import com.diligentgroup.scptastebuds.domain.SelectedYearId;
import com.diligentgroup.scptastebuds.services.YearService;
import com.diligentgroup.scptastebuds.web.mappers.DomainCommandMapper;

@Component
@Order(1)

public class ActiveYearFilter implements Filter {

	@Autowired
	SelectedYearId selectedYear;
	@Autowired
	YearService yearService;
	@Autowired
	DomainCommandMapper mapper;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		Integer currentYearId = Calendar.getInstance().get(Calendar.YEAR);

		if (selectedYear.getYearId() == null) {
			selectedYear.setYearId(currentYearId);
		}
		ActiveYearThreadLocal.set(selectedYear.getYearId());
		request.setAttribute("activeYear", selectedYear.getYearId());
		request.setAttribute("currentYear", currentYearId);
		chain.doFilter(request, response);
		ActiveYearThreadLocal.unset();
	}
}
